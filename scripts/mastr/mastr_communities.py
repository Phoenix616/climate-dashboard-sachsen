import pandas as pd
import json
import mastr_constants as C

# 2954 == geprüft
# 2955 == ungeprüft


def generate_mastr_communities(data_frame):

    total_active = data_frame[data_frame[C.STATE] == C.ACTIVE]

    # foo = total_active.reset_index().groupby("Gemeindeschluessel").resample("M", on=C.DATE)['Bruttoleistung'].sum() \
    #     .groupby(level=0).cumsum().reset_index();
    #
    # print(foo.loc[foo.InbetriebnahmeDatum >= C.PAST_TEN_YEARS].to_dict(orient="records"))

    # filter all values which are installed after the 1st of January this year
    by_this_year = total_active[total_active.InbetriebnahmeDatum >= C.CURRENT_YEAR]
    # group the data by power and sum this up
    by_this_year_grouped_by_community = by_this_year.groupby('Gemeindeschluessel')['Bruttoleistung'].sum().reset_index()
    # name the power column to power in this year
    by_this_year_grouped_by_community['BruttoleistungThisYear'] = by_this_year_grouped_by_community['Bruttoleistung']

    checked = by_this_year.loc[by_this_year.IsNBPruefungAbgeschlossen == 2954]
    checked_grouped = checked.groupby('Gemeindeschluessel')['Bruttoleistung'].sum().reset_index()
    by_this_year_grouped_by_community = pd.merge(by_this_year_grouped_by_community, checked_grouped, on="Gemeindeschluessel", how="left")

    unchecked = by_this_year.loc[by_this_year.IsNBPruefungAbgeschlossen == 2955]
    unchecked_grouped = unchecked.groupby('Gemeindeschluessel')['Bruttoleistung'].sum().reset_index()
    by_this_year_grouped_by_community = pd.merge(by_this_year_grouped_by_community, unchecked_grouped, on="Gemeindeschluessel", how="left")

    by_this_year_grouped_by_community = by_this_year_grouped_by_community.drop(['Bruttoleistung_x'], axis=1)
    by_this_year_grouped_by_community = by_this_year_grouped_by_community.rename(columns={"Bruttoleistung_y": "BruttoleistungThisYearChecked", "Bruttoleistung": "BruttoleistungThisYearUnChecked"})

    ##########
    ###  total stuff

    # get the sum of the total power installed in the community
    by_total_group_by_community= total_active.groupby('Gemeindeschluessel')['Bruttoleistung'].sum().reset_index()
    # rename the column to have it for this the total
    by_total_group_by_community['BruttoleistungTotal'] = by_total_group_by_community['Bruttoleistung']

    checked = total_active.loc[total_active.IsNBPruefungAbgeschlossen == 2954]
    checked_grouped = checked.groupby('Gemeindeschluessel')['Bruttoleistung'].sum().reset_index()
    by_total_group_by_community = pd.merge(by_total_group_by_community, checked_grouped, on="Gemeindeschluessel", how="left")

    unchecked = total_active.loc[total_active.IsNBPruefungAbgeschlossen == 2955]
    unchecked_grouped = unchecked.groupby('Gemeindeschluessel')['Bruttoleistung'].sum().reset_index()
    by_total_group_by_community = pd.merge(by_total_group_by_community, unchecked_grouped, on="Gemeindeschluessel", how="left")

    by_total_group_by_community = by_total_group_by_community.drop(['Bruttoleistung_x'], axis=1)
    by_total_group_by_community = by_total_group_by_community.rename(columns={"Bruttoleistung_y": "BruttoleistungTotalChecked", "Bruttoleistung": "BruttoleistungTotalUnChecked"})

    gb = pd.merge(pd.read_excel("/app/scripts/mastr/einwohnerzahlen-gemeinden-sachsen.xlsx"), by_total_group_by_community, on="Gemeindeschluessel", how="left")
    gb = pd.merge(gb, by_this_year_grouped_by_community, on="Gemeindeschluessel", how="left")

    return json.loads(gb.to_json(orient="records"))
