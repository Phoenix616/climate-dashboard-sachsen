import datetime

WEBAPP_DIRECTORY = "/app/src/main/webapp/app/"
ACTIVE = "In Betrieb"
SUN = "Solare Strahlungsenergie"
DATE = "InbetriebnahmeDatum"
STATE = "BetriebsStatusName"
POWER = "Bruttoleistung"
ENERGY_SOURCE = 'EnergietraegerName'
TIME_UNIT = 'TimeUnit'
BRUTTO_POWER_TOTAL = 'BruttoleistungTotal'
CURRENT_YEAR = '{}-01-01'.format(datetime.date.today().year);
LAST_YEAR = '{}-01-01'.format(datetime.date.today().year - 1)
SECOND_LAST_YEAR = '{}-01-01'.format(datetime.date.today().year - 2)
PAST_TEN_YEARS = '{}-01-01'.format(datetime.date.today().year - 10)
LANDKREISE = [(625, "BAUTZEN"), (511, "CHEMNITZ"), (612, "DRESDEN"), (521, "ERZGEBIRGSKREIS"), (626, "GOERLITZ"), (729, "LANDKREIS_LEIPZIG"), (713, "LEIPZIG"), (627, "MEISSEN"), (522, "MITTELSACHSEN"), (730, "NORDSACHSEN"), (628, "SSOE"), (523, "VOGTLANDKREIS"), (524, "ZWICKAU")]

TRANSLATION = {"sun": SUN, "wind": "Wind", "bio": "Biomasse", "water": "Wasser", "storage": "Speicher"}
