import pandas as pd
import mastr_constants as C
import fileinput
import json
import shutil
from os.path import exists
from mastr_statistics_facts import *
from mastr_communities import *
from mastr_statistics_by_time_unit import *

pd.set_option('display.max_rows', 1000)
pd.set_option('display.min_rows', 100)

# read data from original downloaded file
df = pd.read_csv("/app/data/mastr/mastr_sachsen_original.csv", sep=",", skiprows=0, skipinitialspace=True,
                 parse_dates=[C.DATE, 'DatumLetzteAktualisierung', 'EinheitMeldeDatum'], low_memory=False)
df = df[df.Bundesland == "Sachsen"]
## mastr contains landkreise in sachsen which are not in sachsen
df = df[df['LandkreisId'].isin([625, 511, 612, 521, 626, 729, 713, 627, 522, 730, 628, 523, 524])]

## group sum of installed energy by landkreis
lines_of_file = ""
for key, value in df.groupby(['LandkreisId', 'EnergietraegerName'])[C.POWER].sum().to_dict().items():
    if key[0] != 91 and key[0] !=315: # wrong data
        # find the correct district from the list of districts
        district = [x for x in C.LANDKREISE if x[0] == key[0]][0]
        lines_of_file += (f'{district[1]}.statistics!["{key[1]}"] = new Statistic("{key[1]}", {value}, "kW");\n');

lines_of_file += "// data generation import needle\n\n"

if not exists(C.WEBAPP_DIRECTORY + "config/data/mastr/saxony_districts_data.constants.ts"):
    shutil.copyfile(C.WEBAPP_DIRECTORY + "config/data/saxony_districts_data.constants.ts", C.WEBAPP_DIRECTORY + "config/data/mastr/saxony_districts_data.constants.ts")

with fileinput.FileInput(C.WEBAPP_DIRECTORY + "config/data/mastr/saxony_districts_data.constants.ts", inplace=True) as file:
    for line in file:
        print(line.replace("// data generation import needle", lines_of_file), end='')

### generate difference in different time intervals
result = {};
for energy_source in ["Solare Strahlungsenergie", "Speicher", "Wind"]:
    for group_by in ["W-Mon", "M", "Y"]:
        print("Generating data for '" + energy_source + "' with grouping '" + group_by + "'")
        result[energy_source.lower().replace(" ", "_") + "_" + group_by.lower()] = generate_statistic_by_time_unit(df, energy_source, group_by, '2000-01-01' if group_by == "Y" else '2020-01-01');
        print("Done.")

with open(C.WEBAPP_DIRECTORY + "config/data/mastr/mastr_renewables_by_time_unit.ts", "w", encoding="utf-8") as f:
    f.write("export const RENEWABLES_INCREASE_BY_TIME_UNIT : { [key: string]: any } = ")
    json.dump(result, f, ensure_ascii=False, indent=2)
    f.write(";")

### generate simple facts
facts = {}
for energy_source in ["sun", "wind", "bio", "water", "storage"]:
    facts[energy_source] = generate_statistics(df, C.TRANSLATION[energy_source])

with open(C.WEBAPP_DIRECTORY + "config/data/mastr/mastr_statistics.ts", "w", encoding="utf-8") as f:
    f.write("export const STATISTICS = ")
    json.dump(facts, f, ensure_ascii=False, indent=2)
    f.write(";")

### generate communities data
with open(C.WEBAPP_DIRECTORY + "config/data/mastr/mastr_communities_sun.ts", "w", encoding="utf-8") as f:
    f.write("import {Source} from \"../../../entities/models/source.model\";")
    f.write("export const MASTR_COMMUNITIES_SUN = { communities: ")
    data = df[df.EnergietraegerName == C.TRANSLATION["sun"]]
    json.dump(generate_mastr_communities(data), f, ensure_ascii=False, indent=2)
    f.write(", source: new Source(\"https://www.marktstammdatenregister.de/MaStR\", \
        \"Marktstammdatenregister der Bundesnetzagentur\", \
        \"" + str(max(data[C.DATE])).replace(" 00:00:00", "") + "\", \
        \"daily\"),")
    f.write("}")
    f.write(";")

with open(C.WEBAPP_DIRECTORY + "config/data/mastr/mastr_communities_storage.ts", "w", encoding="utf-8") as f:
    f.write("import {Source} from \"../../../entities/models/source.model\";")
    f.write("export const MASTR_COMMUNITIES_STORAGE = { communities: ")
    data = df[(df.EnergietraegerName == C.TRANSLATION["storage"]) & (df.Batterietechnologie.notnull())]
    json.dump(generate_mastr_communities(data), f, ensure_ascii=False, indent=2)
    f.write(", source: new Source(\"https://www.marktstammdatenregister.de/MaStR\", \
        \"Marktstammdatenregister der Bundesnetzagentur\", \
        \"" + str(max(data[C.DATE])).replace(" 00:00:00", "") + "\", \
        \"daily\"),")
    f.write("}")
    f.write(";")

# some statistics from 2022-10-22

# print(df.groupby(['EnergietraegerName'])['EnergietraegerName'].count())
# EnergietraegerName
# Biomasse                      633
# Braunkohle                     15
# Erdgas                       3004
# Geothermie                      1
# Grubengas                       2
# Klärschlamm                    14
# Mineralölprodukte             401
# Solare Strahlungsenergie    69761
# Solarthermie                    1
# Speicher                    17373
# Wasser                        422
# Wind                          972
# Wärme                          12
# andere Gase                   106
# nicht biogener Abfall           4
