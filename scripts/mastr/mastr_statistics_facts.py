import pandas as pd
import mastr_constants as C


def generate_statistics(data_frame, energy_source):
    filtered_by_energy_source = data_frame[data_frame.EnergietraegerName == energy_source];
    by_weeks = filtered_by_energy_source.groupby('EnergietraegerName').resample('W-Mon', on=C.DATE).sum(numeric_only=True).reset_index().sort_values(by=[C.DATE])
    by_weeks['TimeUnit'] = pd.DatetimeIndex(by_weeks[C.DATE]).year.astype(str) + "-" + by_weeks[C.DATE].dt.isocalendar().week.astype(str).str.zfill(2)

    # this needs to be precalculated in case we just switched to a new year
    this_year = by_weeks[by_weeks[C.DATE] >= C.CURRENT_YEAR][C.POWER].tail(1)

    return {
        "factories-active": len(filtered_by_energy_source.loc[filtered_by_energy_source[C.STATE] == C.ACTIVE].index),
        "factories-new-current-year": len(filtered_by_energy_source.loc[(filtered_by_energy_source[C.STATE] == C.ACTIVE) & (filtered_by_energy_source[C.DATE] >= C.CURRENT_YEAR)].index),
        "factories-new-last-year": len(filtered_by_energy_source.loc[(filtered_by_energy_source[C.STATE] == C.ACTIVE) & (filtered_by_energy_source[C.DATE] >= C.LAST_YEAR) & (filtered_by_energy_source[C.DATE] < C.CURRENT_YEAR)].index),
        "factories-new-second-last-year": len(filtered_by_energy_source.loc[(filtered_by_energy_source[C.DATE] >= C.SECOND_LAST_YEAR) & (filtered_by_energy_source[C.DATE] < C.LAST_YEAR)].index),

        "brutto-power-max-week-current-year": by_weeks[by_weeks[C.DATE] >= C.CURRENT_YEAR][C.POWER].max(),
        "brutto-power-max-week-last-year": by_weeks[(by_weeks[C.DATE] >= C.LAST_YEAR) & (by_weeks[C.DATE] < C.CURRENT_YEAR)][C.POWER].max(),
        "brutto-power-max-week-second-last-year": by_weeks.loc[(by_weeks[C.DATE] >= C.SECOND_LAST_YEAR) & (by_weeks[C.DATE] < C.LAST_YEAR)][C.POWER].max(),

        "brutto-power-avg-week-current-year": by_weeks[by_weeks[C.DATE] >= C.CURRENT_YEAR][C.POWER].mean(),
        "brutto-power-avg-week-last-year": by_weeks.loc[(by_weeks[C.DATE] >= C.LAST_YEAR) & (by_weeks[C.DATE] < C.CURRENT_YEAR)][C.POWER].mean(),
        "brutto-power-avg-week-second-last-year": by_weeks.loc[(by_weeks[C.DATE] >= C.SECOND_LAST_YEAR) & (by_weeks[C.DATE] < C.LAST_YEAR)][C.POWER].mean(),

        "brutto-power-median-week-current-year": by_weeks[by_weeks[C.DATE] >= C.CURRENT_YEAR][C.POWER].median(),
        "brutto-power-median-week-last-year": by_weeks.loc[(by_weeks[C.DATE] >= C.LAST_YEAR) & (by_weeks[C.DATE] < C.CURRENT_YEAR)][C.POWER].median(),
        "brutto-power-median-week-second-last-year": by_weeks.loc[(by_weeks[C.DATE] >= C.SECOND_LAST_YEAR) & (by_weeks[C.DATE] < C.LAST_YEAR)][C.POWER].median(),

        "brutto-power-last-7d-current-year":     0 if len(this_year) == 0 else this_year.item(),
        "brutto-power-last-7d-last-year":        by_weeks.loc[(by_weeks[C.DATE] >= C.LAST_YEAR) & (by_weeks[C.DATE] < C.CURRENT_YEAR)][C.POWER].tail(1).item(),
        "brutto-power-last-7d-second-last-year": by_weeks.loc[(by_weeks[C.DATE] >= C.SECOND_LAST_YEAR) & (by_weeks[C.DATE] < C.LAST_YEAR)][C.POWER].tail(1).item(),
    }
