import pandas as pd
import mastr_constants as C


def generate_statistic_by_time_unit(data_frame, energy_source, group_by, min_date):
    frame = data_frame[(data_frame.EnergietraegerName == energy_source) & (data_frame.InbetriebnahmeDatum >= min_date)]
    frame = frame.groupby(C.ENERGY_SOURCE).resample(group_by, on=C.DATE).sum(numeric_only=True).reset_index().sort_values(by=[C.DATE])
    if group_by == "W-Mon":
        frame[C.TIME_UNIT] = pd.DatetimeIndex(frame[C.DATE]).year.astype(str) + "-" + frame[C.DATE].dt.isocalendar().week.astype(str).str.zfill(2)
    elif group_by == "M":
        frame[C.TIME_UNIT] = pd.DatetimeIndex(frame[C.DATE]).year.astype(str) + "-" + frame[C.DATE].dt.month.apply(lambda x: str(x).zfill(2))
    elif group_by == "Y":
        frame[C.TIME_UNIT] = pd.DatetimeIndex(frame[C.DATE]).year

    frame['BruttoleistungTotal'] = frame[C.POWER].cumsum()
    frame = frame[[C.ENERGY_SOURCE, C.TIME_UNIT, C.BRUTTO_POWER_TOTAL, C.POWER]].sort_values(by=[C.ENERGY_SOURCE, C.TIME_UNIT])
    return frame.to_dict(orient="records")
