from netCDF4 import Dataset
from netCDF4 import num2date
import pandas as pd
import geopandas
import re
from shapely.geometry import shape, Point
from shapely.prepared import prep
from shapely.wkt import loads
import shapely.wkt
import xarray as xr
import json

simpledec = re.compile(r"\d*\.\d+")
def mround(match):
    return "{:.5f}".format(float(match.group()))

# load GeoJSON file containing Sachsen bounds
with open('/app/scripts/ufz/SACHSEN.geojson') as f:
    js = json.load(f)

sachsen = prep(shape(js['features'][0]['geometry']))

for file_name in ['smi_gesamtboden', 'smi_oberboden']:

    print('Loading file: /app/data/ufz/' + file_name + "_daily_14d.nc")
    nc = Dataset("/app/data/ufz/" + file_name + "_daily_14d.nc", mode='r')
    df = xr.open_dataset("/app/data/ufz/" + file_name + "_daily_14d.nc").to_dataframe().reset_index()

    geopandas.options.display_precision = 3

    geo_df = geopandas.GeoDataFrame(df, geometry=geopandas.points_from_xy(df.lon, df.lat))
    geo_df_filtered = geo_df.loc[geo_df.geometry.apply(lambda p: sachsen.contains(p))]
    geo_df_filtered = geo_df_filtered.rename(columns={"lon": "lng", "time": "date"}).drop(['northing', 'easting'], axis=1)

    # we need to switch to 3857 in order to add meters as buffer
    geo_df_filtered.crs = 'epsg:4326'
    geo_df_filtered = geo_df_filtered.to_crs(epsg=3857)
    geo_df_filtered['buffer'] = geo_df_filtered.buffer(3100, cap_style=3).to_crs(epsg=4326)
    geo_df_filtered = geo_df_filtered.drop(['geometry', 'lat', 'lng'], axis=1).rename(columns={"buffer": "geometry"})
    geo_df_filtered.geometry = geo_df_filtered.geometry.apply(lambda x: loads(re.sub(simpledec, mround, x.wkt)))

    datesToGeoJson = {}
    for date in geo_df_filtered['date'].unique():
        geo_df_filtered_date = geo_df_filtered[geo_df_filtered.date == date]
        geo_df_filtered_date = geo_df_filtered_date.assign(date=lambda d: d['date'].dt.strftime('%Y-%m-%d'))
        datesToGeoJson[pd.to_datetime(str(date)).strftime('%Y-%m-%d')] = json.loads(geo_df_filtered_date.to_json())

    with open("/app/src/main/webapp/app/config/data/ufz/" + file_name.upper() + ".ts", "w", encoding="utf-8") as f:
        f.write("export const " + file_name.upper() + " : { [key: string]: any } = ")
        f.write(json.dumps(datesToGeoJson, indent = 1))
        f.write(";")
