import pandas as pd
import bnetza_constants as C


def generate_statistic_by_time_unit(data_frame, group_by, min_date):
    frame = data_frame.groupby("Bundesland").resample(group_by, on=C.DATE).sum(numeric_only=True).reset_index().sort_values(by=[C.DATE])
    if group_by == "W-Mon":
        frame[C.TIME_UNIT] = pd.DatetimeIndex(frame[C.DATE]).year.astype(str) + "-" + frame[C.DATE].dt.isocalendar().week.astype(str).str.zfill(2)
    elif group_by == "M":
        frame[C.TIME_UNIT] = pd.DatetimeIndex(frame[C.DATE]).year.astype(str) + "-" + frame[C.DATE].dt.month.apply(lambda x: str(x).zfill(2))
    elif group_by == "Y":
        frame[C.TIME_UNIT] = pd.DatetimeIndex(frame[C.DATE]).year

    frame['TotalCountChargingPoints'] = frame["Anzahl Ladepunkte"].cumsum()
    frame['TotalChargingPower'] = frame["Anschlussleistung"].cumsum()
    frame = frame[(frame[C.DATE] >= min_date)][[C.TIME_UNIT, "Anzahl Ladepunkte", "Anschlussleistung", "TotalCountChargingPoints", "TotalChargingPower"]].sort_values(by=[C.TIME_UNIT])
    frame = frame.rename(columns={"Anzahl Ladepunkte": "TimeUnitChargingPoints", "Anschlussleistung": "TimeUnitPowerRating"})
    return frame.to_dict(orient="records")
