import pandas as pd
import json
import fileinput
import shutil
import datetime
import bnetza_constants as C
from os.path import exists
from bnetza_statistics_by_time_unit import generate_statistic_by_time_unit
from bnetza_betreiber_ladepunkte import generate_dataset

complete = pd.read_excel("/app/data/bnetza/bnetza-ladesäulen.xlsx", skiprows=7, dtype={'Anschlussleistung': float}, decimal=",")
date = datetime.datetime.strptime(complete.columns[0].replace("Stand: " , ""), '%d.%m.%Y').strftime('%Y-%m-%d')

complete = pd.read_excel("/app/data/bnetza/bnetza-ladesäulen.xlsx", skiprows=10, dtype={'Anschlussleistung': float}, decimal=",")
sachsen = complete.loc[complete["Bundesland"] == "Sachsen"]
betreiber = generate_dataset(sachsen, False)
betreiber_current_year = generate_dataset(sachsen, True)

result = {};
for group_by in ["W-Mon", "M", "Y"]:
    print("Generating data for charging stations with grouping '" + group_by + "'")
    result[group_by.lower()] = generate_statistic_by_time_unit(sachsen, group_by, '2011-01-01' if group_by == "Y" else '2020-01-01');
    print("Done.")

with open("/app/src/main/webapp/app/config/data/bnetza/bnetza_charging_stations_by_time_unit.ts", "w", encoding="utf-8") as f:
    f.write(f"export const BNETZA_DATA_RELEASE_DATE : string = '{date}';\n")
    f.write("export const CHARGING_STATIONS_BY_TIME_UNIT : { [key: string]: any } = ")
    json.dump(result, f, ensure_ascii=False, indent=2)
    f.write(";\nexport const CHARGING_STATIONS_OPERATORS : { [key: string]: any }[] = ")
    json.dump(betreiber.to_dict(orient="records"), f, ensure_ascii=False, indent=2)
    f.write(";")
    f.write(";\nexport const CHARGING_STATIONS_OPERATORS_CURRENT_YEAR : { [key: string]: any }[] = ")
    json.dump(betreiber_current_year.to_dict(orient="records"), f, ensure_ascii=False, indent=2)
    f.write(";")

lines_of_file = ""
for key, value in sachsen.groupby("Kreis/kreisfreie Stadt").sum(numeric_only=True).to_dict().items():
    if key == "Anzahl Ladepunkte" or key == "Anschlussleistung":
        for district in C.LANDKREISE:
            lines_of_file += (f'{district[2]}.statistics!["{key}"] = new Statistic("{key}", {value[district[1]]}, "{ "kW" if "Anschlussleistung" == key else ""}");\n');

lines_of_file += "// data generation import needle\n\n"

if not exists(C.WEBAPP_DIRECTORY + "config/data/mastr/saxony_districts_data.constants.ts"):
    shutil.copyfile(C.WEBAPP_DIRECTORY + "config/data/saxony_districts_data.constants.ts", C.WEBAPP_DIRECTORY + "config/data/mastr/saxony_districts_data.constants.ts")

with fileinput.FileInput(C.WEBAPP_DIRECTORY + "config/data/mastr/saxony_districts_data.constants.ts", inplace=True) as file:
    for line in file:
        print(line.replace("// data generation import needle", lines_of_file), end='')


# frame = sachsen.groupby("Bundesland").resample("M", on="Inbetriebnahmedatum").sum(numeric_only=True)[["Anzahl Ladepunkte", "Anschlussleistung"]]

# print(frame)
# print("------------------------------------------------------------------------------------")
# print("------------------------------------------------------------------------------------")
# print("------------------------------------------------------------------------------------")
# print(sachsen.groupby(["Kreis/kreisfreie Stadt", "Normalladeeinrichtung"])["Anzahl Ladepunkte"].sum(numeric_only=True))
# print("------------------------------------------------------------------------------------")
# print("------------------------------------------------------------------------------------")
# print("------------------------------------------------------------------------------------")
# print(sachsen.groupby(["Kreis/kreisfreie Stadt", "Normalladeeinrichtung"])["Anschlussleistung"].sum(numeric_only=True))
# print("------------------------------------------------------------------------------------")
# print("------------------------------------------------------------------------------------")
# print("------------------------------------------------------------------------------------")
# print(f'Sum, {sachsen["Anzahl Ladepunkte"].sum()}')
# print(f'mean, {sachsen["Anzahl Ladepunkte"].mean()}')
# print(f'min, {sachsen["Anzahl Ladepunkte"].min()}')
# print(f'max, {sachsen["Anzahl Ladepunkte"].max()}')


