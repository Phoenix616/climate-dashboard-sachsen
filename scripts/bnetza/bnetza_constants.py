import datetime

TIME_UNIT = 'TimeUnit'
DATE = "Inbetriebnahmedatum"
WEBAPP_DIRECTORY = "/app/src/main/webapp/app/"
LANDKREISE = [(625, "Landkreis Bautzen", "BAUTZEN"), (511, "Kreisfreie Stadt Chemnitz", "CHEMNITZ"), (612, "Kreisfreie Stadt Dresden", "DRESDEN"), (521, "Landkreis Erzgebirgskreis", "ERZGEBIRGSKREIS"), (626, "Landkreis Görlitz", "GOERLITZ"), (729, "Landkreis Leipzig", "LANDKREIS_LEIPZIG"), (713, "Kreisfreie Stadt Leipzig", "LEIPZIG"), (627, "Landkreis Meißen", "MEISSEN"), (522, "Landkreis Mittelsachsen", "MITTELSACHSEN"), (730, "Landkreis Nordsachsen", "NORDSACHSEN"), (628, "Landkreis Sächsische Schweiz-Osterzgebirge", "SSOE"), (523, "Landkreis Vogtlandkreis", "VOGTLANDKREIS"), (524, "Landkreis Zwickau", "ZWICKAU")]
