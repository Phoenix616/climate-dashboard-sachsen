from datetime import date
import pandas as pd

def generate_dataset(data_frame, only_current_year):

    if only_current_year:
        ##  - 1 is a fix to get around missing data from Bund #TODO REMOVE
        data_frame = data_frame.loc[data_frame["Inbetriebnahmedatum"] >= pd.Timestamp(date.today().year - 1, 1, 1)]

    betreiber = data_frame.groupby("Betreiber", as_index=False).sum(numeric_only=True).sort_values(by=["Anschlussleistung"], ascending=False).reset_index()
    betreiber = betreiber.rename(columns={"Betreiber" : "name"})
    betreiber["Durchschnittliche Ladeleistung"] = betreiber["Anschlussleistung"] / betreiber["Anzahl Ladepunkte"]
    betreiber = betreiber[["name", "Anschlussleistung", "Anzahl Ladepunkte", "Durchschnittliche Ladeleistung"]]
    return betreiber
