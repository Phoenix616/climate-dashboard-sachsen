import pandas as pd
import json

df = pd.read_csv("/app/data/dwd/dwd_air_temperature_mean.csv", sep=";", skiprows=1, skipinitialspace = True)
all = df[["Jahr", "Sachsen"]].copy()
all.rename(columns={"Sachsen":"dwd_air_temperature_mean"}, inplace=True)

new1 = pd.read_csv("/app/data/dwd/dwd_frost_days.csv", sep=";", skiprows=1, skipinitialspace = True)
new1 = new1[["Jahr", "Sachsen"]].copy()
new1.rename(columns={"Sachsen": "dwd_frost_days"}, inplace=True)
all = all.merge(new1, how="left")

new1 = pd.read_csv("/app/data/dwd/dwd_hot_days.csv", sep=";", skiprows=1, skipinitialspace = True)
new1 = new1[["Jahr", "Sachsen"]].copy()
new1.rename(columns={"Sachsen":"dwd_hot_days"}, inplace=True)
all = all.merge(new1, how="left")

new1 = pd.read_csv("/app/data/dwd/dwd_precipGE10mm_days.csv", sep=";", skiprows=1, skipinitialspace = True)
new1 = new1[["Jahr", "Sachsen"]].copy()
new1.rename(columns={"Sachsen": "dwd_precipGE10mm_days"}, inplace=True)
all = all.merge(new1, how="left")

new1 = pd.read_csv("/app/data/dwd/dwd_precipGE20mm_days.csv", sep=";", skiprows=1, skipinitialspace = True)
new1 = new1[["Jahr", "Sachsen"]].copy()
new1.rename(columns={"Sachsen":"dwd_precipGE20mm_days"}, inplace=True)
all = all.merge(new1, how="left")

new1 = pd.read_csv("/app/data/dwd/dwd_precipitation.csv", sep=";", skiprows=1, skipinitialspace = True)
new1 = new1[["Jahr", "Sachsen"]].copy()
new1.rename(columns={"Sachsen":"dwd_precipitation"}, inplace=True)
all = all.merge(new1, how="left")

new1 = pd.read_csv("/app/data/dwd/dwd_hot_days.csv", sep=";", skiprows=1, skipinitialspace = True)
new1 = new1[["Jahr", "Sachsen"]].copy()
new1.rename(columns={"Sachsen":"dwd_hot_days"}, inplace=True)
all = all.merge(new1, how="left")

new1 = pd.read_csv("/app/data/dwd/dwd_summer_days.csv", sep=";", skiprows=1, skipinitialspace = True)
new1 = new1[["Jahr", "Sachsen"]].copy()
new1.rename(columns={"Sachsen":"dwd_summer_days"}, inplace=True)
all = all.merge(new1, how="left")

new1 = pd.read_csv("/app/data/dwd/dwd_sunshine_duration.csv", sep=";", skiprows=1, skipinitialspace = True)
new1 = new1[["Jahr", "Sachsen"]].copy()
new1.rename(columns={"Sachsen":"dwd_sunshine_duration"}, inplace=True)
all = all.merge(new1, how="left")

new1 = pd.read_csv("/app/data/dwd/dwd_tropical_nights_tminGE20.csv", sep=";", skiprows=1, skipinitialspace = True)
new1 = new1[["Jahr", "Sachsen"]].copy()
new1.rename(columns={"Sachsen":"dwd_tropical_nights_tminGE20"}, inplace=True)
all = all.merge(new1, how="left")

result = {
  "years"                                   : all["Jahr"].tolist(),
  "dwd_air_temperature_mean"                : all["dwd_air_temperature_mean"].tolist(),
  "dwd_air_temperature_mean_rolling_30"     : all["dwd_air_temperature_mean"].rolling(30).mean().tolist(),
  "dwd_frost_days"                          : all["dwd_frost_days"].tolist(),
  "dwd_frost_days_rolling_30"               : all["dwd_frost_days"].rolling(30).mean().tolist(),
  "dwd_precipGE10mm_days"                   : all["dwd_precipGE10mm_days"].tolist(),
  "dwd_precipGE10mm_days_rolling_30"        : all["dwd_precipGE10mm_days"].rolling(30).mean().tolist(),
  "dwd_precipGE20mm_days"                   : all["dwd_precipGE20mm_days"].tolist(),
  "dwd_precipGE20mm_days_rolling_30"        : all["dwd_precipGE20mm_days"].rolling(30).mean().tolist(),
  "dwd_precipitation"                       : all["dwd_precipitation"].tolist(),
  "dwd_precipitation_rolling_30"            : all["dwd_precipitation"].rolling(30).mean().tolist(),
  "dwd_summer_days"                         : all["dwd_summer_days"].tolist(),
  "dwd_summer_days_rolling_30"              : all["dwd_summer_days"].rolling(30).mean().tolist(),
  "dwd_hot_days"                            : all["dwd_hot_days"].tolist(),
  "dwd_hot_days_rolling_30"                 : all["dwd_hot_days"].rolling(30).mean().tolist(),
  "dwd_sunshine_duration"                   : all["dwd_sunshine_duration"].tolist(),
  "dwd_sunshine_duration_rolling_30"        : all["dwd_sunshine_duration"].rolling(30).mean().tolist(),
  "dwd_tropical_nights_tminGE20"            : all["dwd_tropical_nights_tminGE20"].tolist(),
  "dwd_tropical_nights_tminGE20_rolling_30" : all["dwd_tropical_nights_tminGE20"].rolling(30).mean().tolist()
}

with open("/app/src/main/webapp/app/config/data/dwd/dwd_total.constants.ts", "w", encoding="utf-8") as f:
    f.write("export const DWD_TOTAL = ")
    json.dump(result, f, ensure_ascii=False, indent=2)
    f.write(";")
