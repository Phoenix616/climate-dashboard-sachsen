export const MOBILITY_CHARTS_CAR_TYPE_QUERY = {
  "queries": [{
    "refId": "BEV",
    "datasource": {
      "uid": "yGt414N7z",
      "type": "mysql"
    },
    "rawSql": "SELECT\n  time AS \"time\",\n  correct_number/1000 AS \"batterieelektrische PKW\"\nFROM fz2\nWHERE\n  Ort = 'Sachsen' AND\n  Typ = \"batterieelektrische PKW\"\nORDER BY time",
    "format": "time_series",
    "datasourceId": 1,
    "intervalMs": 86400000,
    "maxDataPoints": 676
  }, {
    "refId": "PHEV",
    "datasource": {
      "uid": "yGt414N7z",
      "type": "mysql"
    },
    "rawSql": "SELECT\n  time AS \"time\",\n  correct_number/1000 AS \"Plug-in-Hybride\"\nFROM fz2\nWHERE\n  Ort = 'Sachsen' AND\n  Typ = 'Plug-in-Hybrid'\nORDER BY time",
    "format": "time_series",
    "datasourceId": 1,
    "intervalMs": 86400000,
    "maxDataPoints": 676
  }],
  "range": {
    "from": "2017-12-31T23:00:00.000Z",
    "to": "2022-10-29T17:13:21.455Z",
    "raw": {
      "from": "2017-12-31T23:00:00.000Z",
      "to": "now"
    }
  },
  "from": "1514761200000",
  "to": "1667063601455"
}
