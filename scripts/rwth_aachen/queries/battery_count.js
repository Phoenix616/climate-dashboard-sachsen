export const RWTH_AACHEN_BATTERY_COUNT_QUERY = {
  "queries": [{
  "refId": "Heimspeicher",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  anzahl AS \"Heimspeicher\"\nFROM speicher\nWHERE\n  bundesland = 'Sachsen' AND\n  speicherart = \"Heimspeicher\" AND\n  batterietyp = 'Alle Batterietechnologien'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}, {
  "refId": "Gewerbespeicher",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  anzahl AS \"Gewerbespeicher\"\nFROM speicher\nWHERE\n  bundesland = 'Sachsen' AND\n  speicherart = \"Gewerbespeicher\" AND\n  batterietyp = 'Alle Batterietechnologien'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}, {
  "refId": "Großspeicher",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  anzahl AS \"Großspeicher\"\nFROM speicher\nWHERE\n  bundesland = 'Sachsen' AND\n  speicherart = \"Großspeicher\" AND\n  batterietyp = 'Alle Batterietechnologien'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}],
  "range": {
  "from": "2017-10-20T16:25:25.454Z",
    "to": "2022-11-20T17:25:25.455Z",
    "raw": {
    "from": "now-5y",
      "to": "now+1M"
  }
},
  "from": "1508516725454",
  "to": "1668965125455"
}
