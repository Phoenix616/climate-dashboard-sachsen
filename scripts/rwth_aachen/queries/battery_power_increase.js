export const RWTH_AACHEN_BATTERY_POWER_INCREASE_QUERY = {
  "queries": [{
    "refId": "Großspeicher",
    "datasource": {
      "uid": "E1HEhw17z",
      "type": "mysql"
    },
    "rawSql": "SELECT\n  time AS \"time\",\n  nettonennleistung AS \"Großspeicher\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Alle Batterietechnologien' AND\n  speicherart = 'Großspeicher' AND\n  nettonennleistung >= 0\nORDER BY time",
    "format": "time_series",
    "datasourceId": 1,
    "intervalMs": 86400000,
    "maxDataPoints": 676
  }, {
    "refId": "Gewerbespeicher",
    "datasource": {
      "uid": "E1HEhw17z",
      "type": "mysql"
    },
    "rawSql": "SELECT\n  time AS \"time\",\n  nettonennleistung AS \"Gewerbespeicher\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Alle Batterietechnologien' AND\n  speicherart = 'Gewerbespeicher' AND\n  nettonennleistung >= 0\nORDER BY time",
    "format": "time_series",
    "datasourceId": 1,
    "intervalMs": 86400000,
    "maxDataPoints": 676
  }, {
    "refId": "Heimspeicher",
    "datasource": {
      "uid": "E1HEhw17z",
      "type": "mysql"
    },
    "rawSql": "SELECT\n  time AS \"time\",\n  nettonennleistung AS \"Heimspeicher\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Alle Batterietechnologien' AND\n  speicherart = 'Heimspeicher' AND\n  nettonennleistung >= 0\nORDER BY time",
    "format": "time_series",
    "datasourceId": 1,
    "intervalMs": 86400000,
    "maxDataPoints": 676
  }],
  "range": {
    "from": "2017-10-20T15:03:31.502Z",
    "to": "2022-11-20T16:03:31.502Z",
    "raw": {
      "from": "now-5y",
      "to": "now+1M"
    }
  },
  "from": "1508511811502",
  "to": "1668960211502"
}
