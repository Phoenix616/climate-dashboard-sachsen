export const RWTH_AACHEN_BATTERY_MEAN_CAPACITY_QUERY = {
  "queries": [{
  "refId": "Speicherkapazität",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  meanSpeicherkapazitaet AS \"Speicherkapazität\",\n  meanNettonennleistung AS \"Speicherleistung\"\nFROM heim\nWHERE\n  bundesland = 'Sachsen'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}],
  "range": {
  "from": "2017-10-20T17:10:24.510Z",
    "to": "2022-11-20T18:10:24.510Z",
    "raw": {
    "from": "now-5y",
      "to": "now+1M"
  }
},
  "from": "1508519424510",
  "to": "1668967824510"
}
