export const RWTH_AACHEN_BATTERY_TYPE_QUERY = {
  "queries": [{
  "refId": "Lithium-Ionen",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  anteil AS \"Lithium-Ionen\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Lithium-Ionen' AND\n  speicherart = 'Alle Speicher'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}, {
  "refId": "Blei-Säure",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  anteil AS \"Blei-Säure\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Blei-Säure' AND\n  speicherart = 'Alle Speicher'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}, {
  "refId": "Redox-Flow",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  anteil AS \"Redox-Flow\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Redox-Flow' AND\n  speicherart = 'Alle Speicher'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}, {
  "refId": "Hochtemperatur",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  anteil AS \"Hochtemperatur\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Hochtemperatur' AND\n  speicherart = 'Alle Speicher'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}, {
  "refId": "NiCd/NiMH",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  anteil AS \"NiCd/NiMH\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'NiCd / NiMH' AND\n  speicherart = 'Alle Speicher'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}, {
  "refId": "Sonstige",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  anteil AS \"Sonstige\"\nFROM wachstum\nWHERE\n  bundesland = 'Sachsen' AND\n  batterietyp = 'Sonstige' AND\n  speicherart = 'Alle Speicher'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}],
  "range": {
  "from": "2017-10-20T16:25:25.463Z",
    "to": "2022-11-20T17:25:25.463Z",
    "raw": {
    "from": "now-5y",
      "to": "now+1M"
  }
},
  "from": "1508516725463",
  "to": "1668965125463"
}
