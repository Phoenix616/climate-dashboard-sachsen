export const RWTH_AACHEN_BATTERY_POWER_QUERY = {
  "queries": [{
  "refId": "Großspeicher",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  nettonennleistung AS \"Großspeicher\"\nFROM speicher\nWHERE\n  bundesland = 'Sachsen' AND\n  speicherart = 'Großspeicher' AND\n  batterietyp = 'Alle Batterietechnologien'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}, {
  "refId": "Gewerbespeicher",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  nettonennleistung AS \"Gewerbespeicher\"\nFROM speicher\nWHERE\n  bundesland = 'Sachsen' AND\n  speicherart = 'Gewerbespeicher' AND\n  batterietyp = 'Alle Batterietechnologien'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}, {
  "refId": "Heimspeicher",
  "datasource": {
    "uid": "E1HEhw17z",
    "type": "mysql"
  },
  "rawSql": "SELECT\n  time AS \"time\",\n  nettonennleistung AS \"Heimspeicher\"\nFROM speicher\nWHERE\n  bundesland = 'Sachsen' AND\n  speicherart = 'Heimspeicher' AND\n  batterietyp = 'Alle Batterietechnologien'\nORDER BY time",
  "format": "time_series",
  "datasourceId": 1,
  "intervalMs": 86400000,
  "maxDataPoints": 676
}],
  "range": {
  "from": "2017-10-20T14:27:15.438Z",
    "to": "2022-11-20T15:27:15.438Z",
    "raw": {
    "from": "now-5y",
      "to": "now+1M"
  }
},
  "from": "1508509635438",
  "to": "1668958035438"
}
