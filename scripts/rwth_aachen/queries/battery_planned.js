export const RWTH_AACHEN_BATTERY_CAPACITY_PLANNED_QUERY = {
  "queries": [{
    "refId": "Heimspeicher",
    "datasource": {
      "uid": "E1HEhw17z",
      "type": "mysql"
    },
    "rawSql": "SELECT\n  time AS \"time\",\n  batteriekapazitaet AS \"Heimspeicher\"\nFROM zukunft\nWHERE\n  batterietyp = 'Alle Batterietechnologien' AND\n  bundesland = 'Sachsen' AND\n  speicherart = 'Heimspeicher'\nORDER BY time",
    "format": "time_series",
    "datasourceId": 1,
    "intervalMs": 86400000,
    "maxDataPoints": 676
  }, {
    "refId": "Gewerbespeicher",
    "datasource": {
      "uid": "E1HEhw17z",
      "type": "mysql"
    },
    "rawSql": "SELECT\n  time AS \"time\",\n  batteriekapazitaet AS \"Gewerbespeicher\"\nFROM zukunft\nWHERE\n  batterietyp = 'Alle Batterietechnologien' AND\n  bundesland = 'Sachsen' AND\n  speicherart = 'Gewerbespeicher'\nORDER BY time",
    "format": "time_series",
    "datasourceId": 1,
    "intervalMs": 86400000,
    "maxDataPoints": 676
  }, {
    "refId": "Großspeicher",
    "datasource": {
      "uid": "E1HEhw17z",
      "type": "mysql"
    },
    "rawSql": "SELECT\n  time AS \"time\",\n  batteriekapazitaet AS \"Großspeicher\"\nFROM zukunft\nWHERE\n  batterietyp = 'Alle Batterietechnologien' AND\n  bundesland = 'Sachsen' AND\n  speicherart = 'Großspeicher'\nORDER BY time",
    "format": "time_series",
    "datasourceId": 1,
    "intervalMs": 86400000,
    "maxDataPoints": 676
  }],
  "range": {
    "from": "2017-10-20T17:44:46.610Z",
    "to": "2022-11-20T18:44:46.610Z",
    "raw": {
      "from": "now-5y",
      "to": "now+1M"
    }
  },
  "from": "1508521486610",
  "to": "1668969886610"
}
