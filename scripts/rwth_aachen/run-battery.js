import { RWTH_AACHEN_BATTERY_CAPACITY_QUERY } from './queries/battery_capacity.js';
import { RWTH_AACHEN_BATTERY_CAPACITY_INCREASE_QUERY } from './queries/battery_capacity_increase.js';
import { RWTH_AACHEN_BATTERY_POWER_QUERY } from './queries/battery_power.js';
import { RWTH_AACHEN_BATTERY_POWER_INCREASE_QUERY } from './queries/battery_power_increase.js';
import { RWTH_AACHEN_BATTERY_COUNT_QUERY } from './queries/battery_count.js';
import { RWTH_AACHEN_BATTERY_TYPE_QUERY } from "./queries/battery_type.js";
import { RWTH_AACHEN_BATTERY_MEAN_CAPACITY_QUERY } from "./queries/battery_mean_capacity.js";
import { RWTH_AACHEN_BATTERY_CAPACITY_PLANNED_QUERY } from "./queries/battery_planned.js";

import got from 'got';
import * as fs from 'fs';

const GRAFANA_URL = "https://scarica.isea.rwth-aachen.de/mastr/api/ds/query";

const queries = [
  {
    query: RWTH_AACHEN_BATTERY_CAPACITY_QUERY,
    filename : "rwth_aachen_battery_capacity.ts",
    constantName: "RWTH_AACHEN_BATTERY_CAPACITY"
  },
  {
    query: RWTH_AACHEN_BATTERY_CAPACITY_INCREASE_QUERY,
    filename : "rwth_aachen_battery_capacity_increase.ts",
    constantName: "RWTH_AACHEN_BATTERY_CAPACITY_INCREASE"
  },
  {
    query: RWTH_AACHEN_BATTERY_POWER_QUERY,
    filename : "rwth_aachen_battery_power.ts",
    constantName: "RWTH_AACHEN_BATTERY_POWER"
  },
  {
    query: RWTH_AACHEN_BATTERY_POWER_INCREASE_QUERY,
    filename : "rwth_aachen_battery_power_increase.ts",
    constantName: "RWTH_AACHEN_BATTERY_POWER_INCREASE"
  },
  {
    query: RWTH_AACHEN_BATTERY_COUNT_QUERY,
    filename : "rwth_aachen_battery_count.ts",
    constantName: "RWTH_AACHEN_BATTERY_COUNT"
  },
  {
    query: RWTH_AACHEN_BATTERY_TYPE_QUERY,
    filename : "rwth_aachen_battery_type.ts",
    constantName: "RWTH_AACHEN_BATTERY_TYPE"
  },
  {
    query: RWTH_AACHEN_BATTERY_MEAN_CAPACITY_QUERY,
    filename : "rwth_aachen_battery_mean_capacity.ts",
    constantName: "RWTH_AACHEN_BATTERY_MEAN_CAPACITY"
  },
  {
    query: RWTH_AACHEN_BATTERY_CAPACITY_PLANNED_QUERY,
    filename : "rwth_aachen_battery_planned.ts",
    constantName: "RWTH_AACHEN_BATTERY_CAPACITY_PLANNED"
  }
]

const date = new Date();
date.setFullYear( date.getFullYear() - 5 )
const millis = date.getTime();

for (const query of queries) {
  const data = await got.post(GRAFANA_URL, { json: query.query }).json();

  if ( query.constantName === "RWTH_AACHEN_BATTERY_MEAN_CAPACITY" ){
    const newDates = [], newValuesSpeicherKapazitaet = [], newValuesSpeicherLeistung = [];
    for ( let i = 0; i < data.results['Speicherkapazität'].frames[0].data.values[0].length; i++ ) {
      if ( data.results['Speicherkapazität'].frames[0].data.values[0][i] >= millis ) {
        newDates.push(data.results['Speicherkapazität'].frames[0].data.values[0][i]);
        newValuesSpeicherKapazitaet.push(data.results['Speicherkapazität'].frames[0].data.values[1][i]);
        newValuesSpeicherLeistung.push(data.results['Speicherkapazität'].frames[0].data.values[2][i]);
      }
    }
    data.results['Speicherkapazität'].frames[0].data.values[0] = newDates;
    data.results['Speicherkapazität'].frames[0].data.values[1] = newValuesSpeicherKapazitaet;
    data.results['Speicherkapazität'].frames[0].data.values[2] = newValuesSpeicherLeistung;
  }
  else if (query.constantName === "RWTH_AACHEN_BATTERY_CAPACITY_PLANNED") { /* no need to do anything */ }
  else if (query.constantName === "RWTH_AACHEN_BATTERY_TYPE") {
    const newDates = [], newValuesLithiumIon = [], newValuesBleiSaeure = [], newValuesReduxFlow = [];
    const newValuesHochtemperatur = [], newValuesNiCdNiMH = [], newValuesSonstige = [];
    for ( let i = 0; i < data.results['Lithium-Ionen'].frames[0].data.values[0].length; i++ ) {
      if ( data.results['Lithium-Ionen'].frames[0].data.values[0][i] >= millis ) {
        newDates.push(data.results['Lithium-Ionen'].frames[0].data.values[0][i]);
        newValuesLithiumIon.push(data.results['Lithium-Ionen'].frames[0].data.values[1][i]);
        newValuesBleiSaeure.push(data.results['Blei-Säure'].frames[0].data.values[1][i]);
        newValuesReduxFlow.push(data.results['Redox-Flow'].frames[0].data.values[1][i]);
        newValuesHochtemperatur.push(data.results.Hochtemperatur.frames[0].data.values[1][i]);
        newValuesNiCdNiMH.push(data.results['NiCd/NiMH'].frames[0].data.values[1][i]);
        newValuesSonstige.push(data.results['Sonstige'].frames[0].data.values[1][i]);
      }
    }
    data.results['Lithium-Ionen'].frames[0].data.values[0] = newDates;
    data.results['Lithium-Ionen'].frames[0].data.values[1] = newValuesLithiumIon;
    data.results['Blei-Säure'].frames[0].data.values[0] = newDates;
    data.results['Blei-Säure'].frames[0].data.values[1] = newValuesBleiSaeure;
    data.results['Redox-Flow'].frames[0].data.values[0] = newDates;
    data.results['Redox-Flow'].frames[0].data.values[1] = newValuesReduxFlow;
    data.results['Hochtemperatur'].frames[0].data.values[0] = newDates;
    data.results['Hochtemperatur'].frames[0].data.values[1] = newValuesHochtemperatur;
    data.results['NiCd/NiMH'].frames[0].data.values[0] = newDates;
    data.results['NiCd/NiMH'].frames[0].data.values[1] = newValuesNiCdNiMH;
    data.results['Sonstige'].frames[0].data.values[0] = newDates;
    data.results['Sonstige'].frames[0].data.values[1] = newValuesSonstige;
  }
  else {
    const newDates = [], newValuesHeimspeicher = [], newValuesGrosspeicher = [], newValuesGewerbespeicher = [];
    for ( let i = 0; i < data.results.Heimspeicher.frames[0].data.values[0].length; i++ ) {
      if ( data.results.Heimspeicher.frames[0].data.values[0][i] >= millis ) {
        newDates.push(data.results.Heimspeicher.frames[0].data.values[0][i]);
        newValuesHeimspeicher.push(data.results.Heimspeicher.frames[0].data.values[1][i]);
        newValuesGewerbespeicher.push(data.results.Gewerbespeicher.frames[0].data.values[1][i]);
        newValuesGrosspeicher.push(data.results['Großspeicher'].frames[0].data.values[1][i]);
      }
    }
    data.results.Heimspeicher.frames[0].data.values[0] = newDates;
    data.results.Heimspeicher.frames[0].data.values[1] = newValuesHeimspeicher;
    data.results.Gewerbespeicher.frames[0].data.values[0] = newDates;
    data.results.Gewerbespeicher.frames[0].data.values[1] = newValuesGewerbespeicher;
    data.results['Großspeicher'].frames[0].data.values[0] = newDates;
    data.results['Großspeicher'].frames[0].data.values[1] = newValuesGrosspeicher;
  }

  const output = `export const ${query.constantName} = ${JSON.stringify(data, null, 2)};`
  fs.writeFile(`/app/data/rwth_aachen/${query.filename}`, output, function (err,data) {
    if (err) { return console.log(err); }
  });
}


