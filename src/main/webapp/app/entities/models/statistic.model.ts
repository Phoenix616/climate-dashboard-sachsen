export interface IStatistic {
  name: string;
  value: number;
  unit: string;
}

export class Statistic implements IStatistic {
  constructor(
    public name: string,
    public value: number,
    public unit: string,
  ) {}
}
