import {IStatistic} from "./statistic.model";

export interface IDistrict {
  id: number;
  name: string;
  population: number;
  statistics?: {[key: string]: IStatistic};
}

export class District implements IDistrict {
  constructor(
    public id: number,
    public name: string,
    public population: number,
    public statistics?: {[key: string]: IStatistic}
  ) {
    this.statistics = this.statistics ?? {};
  }
}

