import dayjs from "dayjs/esm";

type UpdateInterval = "monthly" | "daily" | "yearly";

export interface ISource {
  url: string;
  label: string;
  lastModified?: string;
  updateInterval?: UpdateInterval;
}

export class Source implements ISource {
  constructor(
    public url: string,
    public label: string,
    public lastModified?: string,
    public updateInterval?: UpdateInterval,
  ) {
  }
}
