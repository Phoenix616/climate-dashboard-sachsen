import {CHARTJS_COLORS, COLOR_BIO, COLOR_SUN} from "../../colors.constants";
import {Source} from "../../../entities/models/source.model";

export const WALDZUSTANDSBERICHT_DATA = {
  source: new Source("https://publikationen.sachsen.de/bdb/artikel/41252", "Sächsisches Staatsministerium für Energie, Klimaschutz, Umwelt und Landwirtschaft", '2022-09-30', "yearly"),
  labels: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007,
          2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015, 2016, 2017, 2018, 2019, 2020, 2021, 2022],
  datasets: {
    alleBaumarten: [
      {
        label: 'Kronenverlichtung (ab 60 Jahre)',
        data: [27.77, 23.93, 25.54, 27.23, 23.49, 23.50, 22.76, 22.86, 24.29, 22.30, 20.59, 20.29, 20.45, 21.76, 21.69, 21.00, 23.20, 20.70, 21.67, 20.01, 20.80, 20.30, 20.80, 20.13, 20.85, 19.52, 20.05, 24.42, 26.52, 30.11, 30.15, 31.56
        ],
        type: 'line',
        stack: 1,
        fill: false,
        borderColor: 'rgb(28,75,75)',
        backgroundColor: 'rgb(28,75,75)',
        pointBackgroundColor: 'rgb(28,75,75)',
        pointBorderColor: 'rgb(255,255,255)'
      },{
        label: 'Kronenverlichtung (bis 60 Jahre)',
        data: [13.30, 12.33, 11.64, 11.85, 9.49 , 10.70, 12.50, 11.93, 12.83, 11.99, 12.90, 13.10, 14.16, 15.24, 13.80, 12.42, 13.60, 12.30, 12.54, 11.64, 11.10, 12.03, 13.00, 11.83, 12.51, 11.69, 10.59, 14.24, 16.92, 18.25, 15.41, 18,46
        ],
        type: 'line',
        stack: 2,
        borderColor: 'rgb(231,113,14)',
        backgroundColor: 'rgb(231,113,14)',
        pointBackgroundColor: 'rgb(231,113,14)',
        pointBorderColor: 'rgb(255,255,255)'
      },{
        label: 'Kronenverlichtung (alle Alter)',
        data: [ 19.05, 17.20, 18.10, 18.67, 15.87, 16.54, 17.13, 16.89, 17.99, 16.68, 16.42, 16.84, 17.11, 18.30, 17.58, 16.58, 18.50, 16.60, 17.35, 16.10, 16.48, 16.64,  17.50, 16.75, 17.64, 16.63, 16.73, 20.91, 23.26, 26.14, 25.12, 27.00],
        type: 'line',
        stack: 3,
        borderColor: 'rgb(155,154,154)',
        backgroundColor: 'rgb(155,154,154)',
        pointBackgroundColor: 'rgb(155,154,154)',
        pointBorderColor: 'rgb(255,255,255)'
      },
      {
        label: 'ungeschädigt',
        data: [37, 39, 41, 40, 46, 52, 44, 44, 42, 44, 40, 41, 37, 34, 36, 41, 35, 42, 42, 43, 43, 43, 37, 39, 37, 44, 43, 31, 26, 21, 24, 22],
        stack: "first",
        type: 'bar',
        borderColor: COLOR_BIO,
        backgroundColor: COLOR_BIO
      },{
        label: 'schwach geschädigt',
        data: [36, 40, 35, 35, 37, 30, 37, 37, 36, 37, 45, 41, 48, 49, 49, 45, 45, 41, 40, 43, 41, 41, 46, 46, 46, 40, 41, 43, 44, 44, 45, 43],
        stack: "first",
        type: 'bar',
        borderColor: COLOR_SUN,
        backgroundColor: COLOR_SUN
      },{
        label: 'deutlich geschädigt',
        data: [27, 21, 24, 25, 17, 18, 19, 19, 22, 19, 15, 18, 15, 17, 15, 14, 20, 17, 18, 14, 16, 16, 17, 15, 17, 16, 16, 26, 30, 35, 31, 35],
        stack: "first",
        type: 'bar',
        borderColor: CHARTJS_COLORS[7],
        backgroundColor: CHARTJS_COLORS[7]
      },

    ]
  }
}
