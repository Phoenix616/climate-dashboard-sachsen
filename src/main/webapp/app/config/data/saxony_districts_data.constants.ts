import { Statistic } from "app/entities/models/statistic.model";
import { District } from "app/entities/models/district.model";

export const BAUTZEN = new District(625, "Bautzen", 302634);
export const CHEMNITZ = new District(511, "Chemnitz", 246855);
export const DRESDEN = new District(612, "Dresden", 551072);
export const ERZGEBIRGSKREIS = new District(521, "Erzgebirgskreis", 340373);
export const GOERLITZ = new District(626, "Görlitz", 256587);
export const LANDKREIS_LEIPZIG = new District(729, "Landkreis Leipzig", 258008);
export const LEIPZIG = new District(713, "Leipzig", 581980);
export const MEISSEN = new District(627, "Meißen", 242862);
export const MITTELSACHSEN = new District(522, "Mittelsachsen", 308153);
export const NORDSACHSEN = new District(730, "Nordsachsen", 197794);
export const SSOE = new District(628, "Sächsische Schweiz-Osterzgebirge", 245418);
export const VOGTLANDKREIS = new District(523, "Vogtlandkreis", 229584);
export const ZWICKAU = new District(524, "Zwickau", 319998);

export const SAXONY_DISTRICTS = [BAUTZEN, CHEMNITZ, DRESDEN, ERZGEBIRGSKREIS, GOERLITZ, LANDKREIS_LEIPZIG,
  LEIPZIG, MEISSEN, MITTELSACHSEN, NORDSACHSEN, SSOE, VOGTLANDKREIS, ZWICKAU]

// data generation import needle












































































































