export const COLOR_BIO: string = 'rgba(75, 192, 192, 1)';
export const COLOR_WATER: string = 'rgba(54, 162, 235, 1)';
export const COLOR_SUN: string = 'rgba(255, 206, 86, 1)';
export const COLOR_WIND: string = 'rgba(255, 99, 132, 1)';
export const COLOR_BLACK: string = 'rgb(0,0,0)';
export const COLOR_ELECTRICITY: string = 'rgb(115,4,250)';
export const INTERPOLATE: ((normalizedValue: number) => string) | "blues" | "brBG" | "buGn" | "buPu" | "cividis" | "cool" | "cubehelixDefault" | "gnBu" | "greens" | "greys" | "inferno" | "magma" | "orRd" | "oranges" | "pRGn" | "piYG" | "plasma" | "puBu" | "puBuGn" | "puOr" | "puRd" | "purples" | "rainbow" | "rdBu" | "rdGy" | "rdPu" | "rdYlBu" | "rdYlGn" | "reds" | "sinebow" | "spectral" | "turbo" | "viridis" | "warm" | "ylGn" | "ylGnBu" | "ylOrBr" | "ylOrRd" | undefined = 'purples';

// https://github.com/valor-software/ng2-charts/blob/master/projects/ng2-charts/src/lib/base-colors.ts
export const CHARTJS_COLORS = [
  "rgb(255, 99, 132)",
  "rgb(54, 162, 235)",
  "rgb(255, 206, 86)",
  "rgb(231, 233, 237)",
  "rgb(75, 192, 192)",
  "rgb(151, 187, 205)",
  "rgb(220, 220, 220)",
  "rgb(247, 70, 74)",
  "rgb(70, 191, 189)",
  "rgb(253, 180, 92)",
  "rgb(148, 159, 177)",
  "rgb(77, 83, 96)"]

export const TRANSPARENT_CHARTJS_COLORS = function(index: number, opacity: number) {
  return [
    "rgba(255, 99, 132)",
    "rgba(54, 162, 235)",
    "rgba(255, 206, 86)",
    "rgba(231,233,237)",
    "rgba(75, 192, 192)",
    "rgba(151, 187, 205)",
    "rgba(220, 220, 220)",
    "rgba(247, 70, 74)",
    "rgba(70, 191, 189)",
    "rgba(253, 180, 92)",
    "rgba(148, 159, 177)",
    "rgba(77, 83, 96)",
  ][index].replace(")", ", " + opacity + ")")
}


