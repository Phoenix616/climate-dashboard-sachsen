import {AfterViewInit, Component, Input, OnInit} from "@angular/core";
import * as ChartGeo from "chartjs-chart-geo";
import {Chart} from "chart.js";
import * as GeoJSON from "geojson";
import _ from "lodash";
import ChartDataLabels from 'chartjs-plugin-datalabels';
import {ChoroplethController, GeoFeature, ColorScale, ProjectionScale, Feature} from 'chartjs-chart-geo';
import {SAXONY_DISTRICTS_WGS84} from "../../../../config/data/saxony_districts_geometry.constants";
import { Topology } from "topojson-specification"
import { District } from "app/entities/models/district.model";
import {COLOR_BLACK, INTERPOLATE} from "app/config/colors.constants";
import {DecimalPipe} from "@angular/common";
Chart.register(ChoroplethController, GeoFeature, ColorScale, ProjectionScale, ChartDataLabels);

@Component({
  selector: 'jhi-saxony-map',
  templateUrl: './saxony-map.component.html',
  styleUrls: ['./saxony-map.component.scss']
})
export class SaxonyMapComponent implements AfterViewInit, OnInit {

  @Input() unit: string = "kW";
  @Input() statistic: string = "Solare Strahlungsenergie";
  @Input() districts: District[] = [];
  @Input() normalizeByPopulation: boolean = true;
  @Input() showTable: boolean = false;
  @Input() showValues: boolean = false;
  @Input() digitsInfo: string = '1.1-2';
  mapDivId: string = (Math.random() + 1).toString(36).substring(2);
  chart!: Chart;
  states: Feature[] = [];
  interpolate = INTERPOLATE;
  predicate: string = "value";
  ascending: boolean = true;
  data: { feature: any; district : District, value: number }[] = [];

  ngOnInit(): void {
    this.prepareData();
  }

  prepareData(): void {
    const topology: Topology = (SAXONY_DISTRICTS_WGS84 as unknown) as Topology;
    this.states = (ChartGeo.topojson.feature(topology, topology.objects.gj) as GeoJSON.FeatureCollection).features;
    this.data = this.states.map(d => {
      const district = this.districts.find(district => d.properties!.id === district.id);
      return {
        feature: d,
        district: district!,
        name: d.properties.name,
        value: district!.statistics![this.statistic].value / (this.normalizeByPopulation ? district!.population : 1) };
    });
    this.data = _.orderBy(this.data, [function(o) { return o.value; }], ['desc'])
  }
  // we need to use this method since otherwise the canvas div would not be ready
  ngAfterViewInit(): void { this.draw(); }

  draw() {

    this.prepareData();
    if ( this.chart ) { this.chart.destroy();  }
    const canvas = <HTMLCanvasElement>document.getElementById(this.mapDivId);
    const ctx = canvas.getContext('2d')!;
    this.chart = new Chart(ctx, {
      type: 'choropleth',
      data: {
        labels: this.states.map((d) => d.properties!.name),
        datasets: [{
          label: 'Kreise und kreisfreie Städte',
          outline: this.states,
          data: this.data,
          borderColor: COLOR_BLACK
        }]
      },
      options: {
        plugins: {
          legend: {display: false},
          datalabels: {
            display: this.showValues,
            backgroundColor: "rgba(187,162,253,1)",
            borderRadius: 2,
            borderColor: "#000",
            color: 'white',
            font: { weight: 'bold' },
            formatter: (v) => { return new DecimalPipe('de').transform(v.value, this.digitsInfo) },
            padding: 3
          },
          tooltip: {
            position: 'nearest',
            callbacks: {
              label: (context: any) => {
                return `${context.dataset.data[context.dataIndex].feature.properties.name ?? ''}:
                  ${
                    new DecimalPipe('de').transform(context.raw.value, this.digitsInfo) ?? 'N/A'
                  } ${this.getUnit()}`;
              }
            }
          }
        },
        scales: {

          xy: {
            projection: 'mercator',
            padding: 0
          },
          color: {
            interpolate: this.interpolate,
            quantize: 10,
            legend: {
              position: 'bottom-right',
              align: 'bottom'
            },
          }
        },
      }
    });
  }

  trackIdentity(_index: number, item: any): number {
    return item.district.id!;
  }

  getUnit() {
    return ` ${this.unit}${this.normalizeByPopulation ? "/Person" : ""}`;
  }

  sort(): void {
    this.data = this.data.sort((a : any, b : any) => {
      if (a[this.predicate] < b[this.predicate]) {
        return this.ascending ? -1 : 1;
      } else if (a[this.predicate] > b[this.predicate]) {
        return this.ascending ? 1 : -1;
      }
      return 0;
    });
  }
}
