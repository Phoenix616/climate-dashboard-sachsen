import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SaxonyMapComponent } from './saxony-map.component';

describe('SaxonyMapComponent', () => {
  let component: SaxonyMapComponent;
  let fixture: ComponentFixture<SaxonyMapComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SaxonyMapComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SaxonyMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
