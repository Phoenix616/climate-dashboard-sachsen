import {Component, Input} from '@angular/core';

@Component({
  selector: 'jhi-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent {

  @Input() threshold = 1.5;
  @Input() valueNumberFormat = "1.1-1";
  @Input() value!: number;
  @Input() diff!: number;
  @Input() name!: string;
  @Input() unit!: string;
  @Input() icon!: string;
  @Input() iconBackgroundColorClass!: string;
  @Input() backgroundColor!: string;

  getTextColor() {
    if ( this.diff >= this.threshold )
      return "text-success";
    else if ( this.diff <= -this.threshold )
      return "text-danger";
    else
      return "text-warning";
  }

  getArrowIcon() {
    if ( this.diff >= this.threshold )
      return 'circle-arrow-up';
    else if ( this.diff <= -this.threshold )
      return 'circle-arrow-down';
    else
      return 'circle-arrow-right';
  }
}
