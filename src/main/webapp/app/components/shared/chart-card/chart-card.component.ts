import {AfterContentInit, Component, ContentChild, ElementRef, Input, OnInit} from '@angular/core';
import {ISource} from "../../../entities/models/source.model";
import {ChartType} from "chart.js";

@Component({
  selector: 'jhi-chart-card',
  templateUrl: './chart-card.component.html',
  styleUrls: ['./chart-card.component.scss']
})
export class ChartCardComponent implements OnInit, AfterContentInit {

  @Input() id: any;
  @Input() heading: any;
  @Input() height: number = 200;
  @Input() chartData: any;
  @Input() chartOptions: any;
  @Input() sources!: ISource[];
  @Input() chartType: ChartType = "line";

  @ContentChild('description') description!: ElementRef;

  constructor() { }

  ngOnInit(): void {
  }

  ngAfterContentInit() {
    console.log(this.description);
  }
}
