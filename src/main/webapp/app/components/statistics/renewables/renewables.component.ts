import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {IconDefinition} from "@fortawesome/fontawesome-common-types";
import {DataService} from "../../../shared/service/data.service";
import {DecimalPipe} from "@angular/common";
import {COLOR_BIO, COLOR_SUN, COLOR_WATER, COLOR_WIND} from 'app/config/colors.constants';
import {District} from "../../../entities/models/district.model";
import {SAXONY_DISTRICTS} from "../../../config/data/mastr/saxony_districts_data.constants";

@Component({
  selector: 'jhi-renewables',
  templateUrl: './renewables.component.html',
  styleUrls: ['./renewables.component.scss']
})
export class RenewablesComponent implements OnInit {

  today: Date = new Date();
  timeUnits: any[] = [];
  selectedTimeUnit: "y" | "m" | "w-mon" = "y";
  sunValue!: number;
  sunDiff!: number;
  windValue!: number;
  windDiff!: number;
  bioValue!: number;
  bioDiff!: number;
  waterValue!: number;
  waterDiff!: number;
  mostRecentYear!: number;
  lastYearGrossContribution!: number;
  districts: District[] = SAXONY_DISTRICTS;
  STATISTICS!: any;
  mastrChartData: any;
  mastrChartOptions: any;
  workChartData: any;
  workChartOptions: any;
  powerChartData: any;
  powerChartOptions: any;

  constructor(private dataService: DataService) {
  }

  ngOnInit(): void {
    this.sunValue = this.dataService.getLatestValue("work", "sun");
    this.sunDiff = this.dataService.getLatestValueDiff("work","sun");;
    this.windValue = this.dataService.getLatestValue("work","wind");
    this.windDiff = this.dataService.getLatestValueDiff("work","wind");;
    this.bioValue = this.dataService.getLatestValue("work","bio");
    this.bioDiff = this.dataService.getLatestValueDiff("work","bio");;
    this.waterValue = this.dataService.getLatestValue("work","water");
    this.waterDiff = this.dataService.getLatestValueDiff("work","water");;
    this.mostRecentYear = this.dataService.getMostRecentYear("work");
    this.lastYearGrossContribution = 22.8;
    this.STATISTICS = this.dataService.getStatistics();
    this.timeUnits = [{ key: "y", value: "Jahr" }, { key: "m", value: "Monat" }, { key: "w-mon", value: "Woche" }];
    this.selectedTimeUnit = this.timeUnits[0].key;
    this.workChartData = this.getChartData("work");
    this.workChartOptions = this.getChartOptions("work");
    this.powerChartData = this.getChartData("power");
    this.powerChartOptions = this.getChartOptions("power");
    this.mastrChartData = this.getMastrChartData();
    this.mastrChartOptions = this.getMastrChartOptions();
  }

  getSumForYear(physicalType: "work" | "power", year: number) {
    return this.dataService.getSumForYear(physicalType, year);
  }

  getChartOptions(physicalType: "power" | "work"): any {
    return {
      scales: { x: { stacked: true }, y: { stacked: true } },
      interaction: {  intersect: false, mode: 'index' },
      plugins: {
        legend: { display: true, position: 'top' },
        datalabels: {
          color: '#36A2EB',
          display: false
        },
        tooltip: { position: 'nearest', callbacks: {
          label(context : any) {
            return `${context.dataset.label ?? ''}: ${
              new DecimalPipe('de').transform(Number(context.formattedValue.replace(/,/g, '')), '1.1-1') ?? 'N/A'
            } ${physicalType === 'work' ? 'GWh' : 'MW'}`;
          },
          footer: (tooltipItems : any) => {
            let sum = 0;
            tooltipItems.forEach(function(tooltipItem : any) { sum += tooltipItem.parsed.y; });
            return 'Summe: ' + sum.toFixed(1) + (physicalType === 'work' ? ' GWh' : ' MW');
          }
        }}
      }
    };
  }

  getChartData(physicalType: "power" | "work") {
    return {
      labels: this.dataService.getLabels(physicalType),
      datasets: [
        { label: 'Biomasse/Gas', data: this.dataService.getData(physicalType, "bio"), borderColor: COLOR_BIO, backgroundColor: COLOR_BIO},
        { label: 'Wasser', data: this.dataService.getData(physicalType, "water"), backgroundColor: COLOR_WATER },
        { label: 'Sonne', data: this.dataService.getData(physicalType, "sun"), backgroundColor: COLOR_SUN },
        { label: 'Wind', data: this.dataService.getData(physicalType, "wind"), backgroundColor: COLOR_WIND }
      ]
    };
  }

  getMastrChartData() {
    this.mastrChartData = {
      labels: this.dataService.getLabels2(this.selectedTimeUnit),
      datasets: [
        {
          label: 'Photovoltaik',
          data: this.dataService.getData2(this.selectedTimeUnit).map((x : number) => x / 1000),
          borderColor: COLOR_SUN,
          backgroundColor: COLOR_SUN},
      ]
    };
    return this.mastrChartData;
  }

  getMastrChartOptions(): any {
    return {
      locale: "de-DE",
      plugins : {
        legend: { display: false },
        datalabels: {
          color: '#36A2EB',
          display: false
        },
        tooltip: { position: 'nearest',
          callbacks: {
            label(context : any) {
              return `${context.dataset.label ?? ''}: ${
                new DecimalPipe('de').transform(Number(context.raw), '1.1-1') ?? 'N/A'
              } MW`;
            }
          }
        }
      },
      parsing: {
        xAxisKey: 'TimeUnit',
        yAxisKey: 'Bruttoleistung'
      },
      interaction: {
        intersect: false,
        mode: 'index',
      }
    };
  }
}
