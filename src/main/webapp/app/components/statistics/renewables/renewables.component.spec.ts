import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RenewablesComponent } from './renewables.component';

describe('RenewablesComponent', () => {
  let component: RenewablesComponent;
  let fixture: ComponentFixture<RenewablesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RenewablesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(RenewablesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
