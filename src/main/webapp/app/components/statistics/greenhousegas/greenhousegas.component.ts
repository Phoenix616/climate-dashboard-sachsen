import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../shared/service/data.service";
import {DecimalPipe} from "@angular/common";

@Component({
  selector: 'jhi-greenhousegas',
  templateUrl: './greenhousegas.component.html',
  styleUrls: ['./greenhousegas.component.scss']
})
export class GreenhousegasComponent implements OnInit {

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  getChartData(): any {
    return this.dataService.getGreenHouseGasData();
  }

  getChartOptions(type: "sachsen" | "atmosphere"): any {
    const options = {
      scales: { x: { stacked: true }, y: { stacked: true } },
      interaction: {  intersect: false, mode: 'index' },
      plugins: {
        legend: { display: true, position: 'top' },
        datalabels: { display: false },
        tooltip: { position: 'nearest', callbacks: {
            label(context : any) {
              return `${context.dataset.label ?? ''}: ${
                new DecimalPipe('de').transform(Number(context.formattedValue.replace(/,/g, '')), '1.1-1') ?? 'N/A'
              } ${type === 'sachsen' ? 't CO2/Jahr' : 'ppm'}`;
            }
          }}
      }
    };
    if ( type === "sachsen" ) {
      // @ts-ignore
      options.plugins.tooltip.callbacks.footer = (tooltipItems: any) => {
        let sum = 0;
        tooltipItems.forEach(function (tooltipItem: any) { sum += tooltipItem.parsed.y; });
        return 'Summe: ' + new DecimalPipe('de').transform(sum, '1.1-1') + " t CO2/Jahr";
      };
    }
    return options;
  }

  getCo2InAtmosphereData(): any {
    return this.dataService.getCo2InAtmosphereData();
  }

  getCo2InAtmosphereOptions(): any {
    return {
      plugins : {
        legend: {
          position : 'top'
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      }
    }
  }
}
