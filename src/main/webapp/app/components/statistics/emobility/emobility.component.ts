import { Component, OnInit } from '@angular/core';
import {
  CHARTJS_COLORS,
  COLOR_BIO,
  COLOR_ELECTRICITY,
  COLOR_SUN,
  TRANSPARENT_CHARTJS_COLORS
} from "../../../config/colors.constants";
import {DatePipe, DecimalPipe} from "@angular/common";
import {DataService} from "../../../shared/service/data.service";
import {SAXONY_DISTRICTS} from "../../../config/data/mastr/saxony_districts_data.constants";
import {District} from "../../../entities/models/district.model";
import {
  BNETZA_DATA_RELEASE_DATE,
  CHARGING_STATIONS_OPERATORS_CURRENT_YEAR
} from "../../../config/data/bnetza/bnetza_charging_stations_by_time_unit";
import {CHARGING_STATIONS_OPERATORS} from "../../../config/data/bnetza/bnetza_charging_stations_by_time_unit";

@Component({
  selector: 'jhi-emobility',
  templateUrl: './emobility.component.html',
  styleUrls: ['./emobility.component.scss']
})
export class EmobilityComponent implements OnInit {

  selectedTimeUnit: "y" | "m" | "w-mon" = "y";
  timeUnits: any[] = [{ key: "y", value: "Jahr" }, { key: "m", value: "Monat" }, { key: "w-mon", value: "Woche" }];
  bnetzaChargingPointsChartData: any;
  bnetzaChargingPointsChartOptions: any;
  bnetzaChargingPowerChartData: any;
  bnetzaChargingPowerChartOptions: any;
  timeUnitChargingPointsYearData: number[] = [];
  timeUnitChargingPowerYearData: number[] = [];
  chartData: any = {};
  COLOR_ELECTRICITY = COLOR_ELECTRICITY;
  yearLabels: number[] = [];
  districts: District[] = SAXONY_DISTRICTS;
  BNETZA_DATA_RELEASE_DATE = BNETZA_DATA_RELEASE_DATE;
  CHARGING_STATIONS_OPERATORS = CHARGING_STATIONS_OPERATORS;
  CHARGING_STATIONS_OPERATORS_CURRENT_YEAR = CHARGING_STATIONS_OPERATORS_CURRENT_YEAR;

  constructor(private dataService: DataService) {}

  ngOnInit(): void {

    // data
    this.bnetzaChargingPointsChartData = this.getBNetzAChartData("TimeUnitChargingPoints");
    this.bnetzaChargingPowerChartData = this.getBNetzAChartData("TimeUnitPowerRating");

    // options
    this.bnetzaChargingPointsChartOptions = this.getBNetzAChartOptions("TimeUnitChargingPoints");
    this.bnetzaChargingPowerChartOptions = this.getBNetzAChartOptions("TimeUnitPowerRating");

    for ( const key of ['MOBILITY_CHARTS_CAR_TYPE']) {
      this.chartData[key] = {};
      this.chartData[key].config = this.getMastrChartOptions(key)
      this.chartData[key].data   = this.dataService.getRWTHAachenMobilityData(key)
    }

    this.timeUnitChargingPointsYearData = this.dataService.getBNetzAData("y", "TotalCountChargingPoints")
    this.timeUnitChargingPowerYearData = this.dataService.getBNetzAData("y", "TotalChargingPower")
    this.yearLabels = this.dataService.getBNetzALabels("y")
  }

  getBNetzAChartData(variableToDisplay: "TimeUnitChargingPoints" | "TimeUnitPowerRating" | "TotalCountChargingPoints" | "TotalChargingPower" ) {
    return {
      labels: this.dataService.getBNetzALabels(this.selectedTimeUnit),
      datasets: [
        {
          label: variableToDisplay === "TimeUnitChargingPoints" ? 'Anzahl an Ladepunkten' : 'Zubauleistung',
          data: this.dataService.getBNetzAData(this.selectedTimeUnit, variableToDisplay),
          borderColor: COLOR_ELECTRICITY,
          backgroundColor: COLOR_ELECTRICITY
        }
      ]
    };
  }

  getBNetzAChartOptions(variableToDisplay: "TimeUnitChargingPoints" | "TimeUnitPowerRating" | "TotalCountChargingPoints" | "TotalChargingPower" ): any {
    return {
      locale: "de-DE",
      plugins : {
        legend: { display: false },
        datalabels: {
          color: '#36A2EB',
          display: false
        },
        tooltip: { position: 'nearest',
          callbacks: {
            label(context : any) {
              return `${context.dataset.label ?? ''}: ${
                new DecimalPipe('de').transform(context.raw, variableToDisplay === "TimeUnitChargingPoints" ? "1.0" : "1.1-1") ?? 'N/A'
              } ${variableToDisplay === "TimeUnitChargingPoints" ? "" : "kW"}`;
            }
          }
        }
      },
      parsing: {
        xAxisKey: 'TimeUnit',
        yAxisKey: variableToDisplay
      },
      interaction: {
        intersect: false,
        mode: 'index',
      }
    };
  }

  getMastrChartOptions(rwthAachenDataKey: string) {
    const unit = `${rwthAachenDataKey === 'TimeUnitChargingPoints' ? "" : ( rwthAachenDataKey === 'TimeUnitPowerRating' ? "kW"
      : ( rwthAachenDataKey === 'XXX' ? "" : "") ) }`;

    let config = {
      interaction: {  intersect: false, mode: 'index' },
      scales: { x: { stacked: true, type: 'time', time : { displayFormats: { month: 'yyyy-MM' }} }, y: { stacked: true } },
      elements: { point:{ radius: 0 } },
      plugins: {
        legend: { display: true, position: 'top' },
        datalabels: { display: false },
        tooltip: { position: 'nearest', callbacks: {
            title(context : any) {
              return new DatePipe('de').transform(context[0].parsed.x, "d. MMMM y");
            },
            label(context : any) {
              return context.dataset.label + `: ${new DecimalPipe('de').transform(context.raw, '1.0')?.trim() ?? 'N/A'} ${unit.trim()}`;
            }
          }}
      }
    }
    return config;
  }
}
