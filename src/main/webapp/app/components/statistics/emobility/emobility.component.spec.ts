import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EmobilityComponent } from './emobility.component';

describe('EmobilityComponent', () => {
  let component: EmobilityComponent;
  let fixture: ComponentFixture<EmobilityComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EmobilityComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EmobilityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
