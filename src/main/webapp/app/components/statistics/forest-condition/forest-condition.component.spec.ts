import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ForestConditionComponent } from './forest-condition.component';

describe('ForestConditionComponent', () => {
  let component: ForestConditionComponent;
  let fixture: ComponentFixture<ForestConditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForestConditionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ForestConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
