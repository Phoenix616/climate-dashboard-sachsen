import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../shared/service/data.service";
import {DecimalPipe} from "@angular/common";

@Component({
  selector: 'jhi-forest-condition',
  templateUrl: './forest-condition.component.html',
  styleUrls: ['./forest-condition.component.scss']
})
export class ForestConditionComponent implements OnInit {

  forestConditionData: any;
  sortedData: any;
  index: any;

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.forestConditionData = this.dataService.getWaldzustandsBerichtData("alleBaumarten")
    this.sortedData= this.forestConditionData.labels
      .map((year: number, index: number) =>
        ({  year: year,
            value: this.forestConditionData.datasets[2].data[index]
        })
      ).sort((a : any, b: any) => b.value - a.value);
    this.index = this.sortedData.findIndex((e: any) => e.value == this.forestConditionData.datasets[2].data[this.forestConditionData.datasets[2].data.length - 1]) + 1
  }

  getSortedDataString(): string {
    return `${this.sortedData[0].year} (${new DecimalPipe('de').transform(this.sortedData[0].value, '1.1-1')} Prozent), ` +
           `${this.sortedData[1].year} (${new DecimalPipe('de').transform(this.sortedData[1].value, '1.1-1')} Prozent) und ` +
           `${this.sortedData[2].year} (${new DecimalPipe('de').transform(this.sortedData[2].value, '1.1-1')} Prozent)`;
  }

  getWaldzustandsBerichtData(): any {
    return this.forestConditionData;
  }

  getChartOptions(): any {
    return {
      scales: { y: { stacked: true } },
      interaction: {  intersect: false, mode: 'index' },
      plugins: {
        legend: { display: true, position: 'top' },
        datalabels: { display: false },
        tooltip: { position: 'nearest', callbacks: {
            label(context : any) {
              return `${context.dataset.label ?? ''}: ${
                new DecimalPipe('de').transform(context.raw, '1.1-1') ?? 'N/A'
              } %`;
            }
          }}
      }
    };
  }
}
