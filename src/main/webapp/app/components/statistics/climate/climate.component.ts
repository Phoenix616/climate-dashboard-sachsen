import { Component, OnInit } from '@angular/core';
import {DataService} from "../../../shared/service/data.service";
import {DWD_TOTAL} from "../../../config/data/dwd/dwd_total.constants";

@Component({
  selector: 'jhi-climate',
  templateUrl: './climate.component.html',
  styleUrls: ['./climate.component.scss']
})
export class ClimateComponent implements OnInit {

  data : any = {
    years: DWD_TOTAL.years.slice(-42),
    frost: DWD_TOTAL.dwd_frost_days_rolling_30.slice(-42),
    summer: DWD_TOTAL.dwd_summer_days_rolling_30.slice(-42),
    heat: DWD_TOTAL.dwd_hot_days_rolling_30.slice(-42)
  }
  lastYearValueHeat  = this.data['heat'][0];
  firstYearValueHeat = this.data['heat'][this.data['heat'].length - 1];
  lastYearValueSummer  = this.data['summer'][0];
  firstYearValueSummer = this.data['summer'][this.data['summer'].length - 1];
  lastYearValueFrost  = this.data['frost'][0];
  firstYearValueFrost = this.data['frost'][this.data['frost'].length - 1];

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
  }

  getChartData(): any {
    return this.dataService.getAverageTemperaturData();
  }

  getChartOptions(): any {
    return {
      plugins : {
        legend: {
          position : 'top',
          display: false
        },
        datalabels: {
          display: false
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      radius: 0,
      fill: false
    };
  }

  getDayChangesData() {
    return {
      labels: this.data.years,
      datasets: [
        {
          label: 'Frosttage (30 Jahre Mittel)',
          data: this.data.frost,
          backgroundColor: '#b7475f',
          borderColor: '#b7475f',
          fill: false,
          hidden: true,
        },{
          label: 'Sommertage (30 Jahre Mittel)',
          data: this.data.summer,
          backgroundColor: '#c04a00',
          borderColor: '#c04a00',
          fill: false,
          hidden: true
        },{
          label: 'Heiße Tage (30 Jahre Mittel)',
          data: this.data.heat,
          backgroundColor: 'rgba(25, 1, 132, 1)',
          borderColor: 'rgba(25, 1, 132, 1)',
          fill: false
        }]
    };
  }

  getDayChangesOptions(): any {
    return {
      plugins : {
        title: {
          display: true,
          text: 'Anzahl der Frost, Sommer und Heißen Tage im Jahr'
        },
        legend: {
          position : 'bottom'
        },
        datalabels: {
          display: false
        }
      },
      interaction: {
        intersect: false,
        mode: 'index',
      },
      radius: 0,
      fill: false,
      x : {
        type: 'linear'
      }
    };
  }
}
