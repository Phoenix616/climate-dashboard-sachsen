import {ChangeDetectorRef, Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {latLng, LatLng, Layer, LeafletEvent, tileLayer} from 'leaflet';
import * as L from 'leaflet';
import "leaflet.vectorgrid";
import {Feature} from "chartjs-chart-geo";
import {SMI_GESAMTBODEN} from "../../../config/data/ufz/SMI_GESAMTBODEN";
import {SMI_OBERBODEN} from "../../../config/data/ufz/SMI_OBERBODEN";
import {Source} from "../../../entities/models/source.model";

@Component({
  selector: 'jhi-water',
  templateUrl: './water.component.html',
  styleUrls: ['./water.component.scss']
})
export class WaterComponent {

  source = new Source("https://www.ufz.de/index.php?de=37937", "UFZ-Dürremonitor - Helmholtz-Zentrum für Umweltforschung");
  selectedDateGesamtboden : string = "";
  possibleDatesGesamtboden : string[] = [];
  selectedDateOberboden : string = "";
  possibleDatesOberboden : string[] = [];

  SMI_GESAMTBODEN = {
    layersControl : {
      baseLayers: {
        'Open Street Map': tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: 'UFZ-Dürremonitor / Helmholtz-Zentrum für Umweltforschung' }),
      }
    },
    options : { zoom: 8, center: latLng(50.9, 13.45), maxZoom: 11 },
    layers : [] as any[],
    GEO_JSON : {} as any,
    CURRENT_SMI : 0
  }
  SMI_OBERBODEN = {
    layersControl : {
      baseLayers: {
        'Open Street Map': tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', { maxZoom: 18, attribution: 'UFZ-Dürremonitor / Helmholtz-Zentrum für Umweltforschung' }),
      }
    },
    options : { zoom: 8, center: latLng(50.9, 13.45), maxZoom: 11 },
    layers : [] as any[],
    GEO_JSON : {} as any,
    CURRENT_SMI : 0
  }

  constructor(private ref: ChangeDetectorRef) {

    let datesGesamtBoden = Object.keys(SMI_GESAMTBODEN);
    this.possibleDatesGesamtboden = datesGesamtBoden.sort((a,b) => Date.parse(b) - Date.parse(a));
    this.selectedDateGesamtboden = this.possibleDatesGesamtboden[0];
    let datesOberBoden = Object.keys(SMI_OBERBODEN);
    this.possibleDatesOberboden = datesOberBoden.sort((a,b) => Date.parse(b) - Date.parse(a));
    this.selectedDateOberboden = this.possibleDatesOberboden[0];

    this.SMI_GESAMTBODEN.GEO_JSON = this.getGeoJSON(this.selectedDateGesamtboden, "GESAMTBODEN");
    this.SMI_GESAMTBODEN.layers = [this.SMI_GESAMTBODEN.GEO_JSON]

    this.SMI_OBERBODEN.GEO_JSON = this.getGeoJSON(this.selectedDateOberboden, "OBERBODEN");
    this.SMI_OBERBODEN.layers = [this.SMI_OBERBODEN.GEO_JSON];
  }

  getGeoJSON(date: string, type : "GESAMTBODEN" | "OBERBODEN" ) {
    return L.geoJSON(type == "GESAMTBODEN" ? SMI_GESAMTBODEN[date] as any : SMI_OBERBODEN[date] as any, {
      style: feature => {
        return {
          'fillColor': this.getColor(feature!.properties.SMI),
          'weight': 3,
          'opacity': 1,
          'color': this.getColor(feature!.properties.SMI),
          'dashArray': '0',
          'fillOpacity': 0.7
        };
      },
      onEachFeature: (feature: Feature, layer: Layer) => {
        layer.on({
          mouseover: (e: any) => {
            const layer = e.target;
            layer.setStyle({
              weight: 5,
              color: '#666',
              dashArray: '',
              fillOpacity: 0.7
            });

            // we have to do this, since this change is not detected by angular
            if ( type == "GESAMTBODEN" ) {
              this.SMI_GESAMTBODEN.CURRENT_SMI = e.target.feature.properties.SMI;
            }
            else {
              this.SMI_OBERBODEN.CURRENT_SMI = e.target.feature.properties.SMI;
            }
            this.ref.detectChanges();

            layer.bringToFront();
          },
          mouseout: (e: any) => {
            if ( type == "GESAMTBODEN" ) {
              this.SMI_GESAMTBODEN.GEO_JSON.resetStyle(e.target);
            }
            else {
              this.SMI_OBERBODEN.GEO_JSON.resetStyle(e.target);
            }
          },
        });
      }
    });
  }

  getColor(d : number) : string {
    if ( d >= 0.2 && d < 0.3 ) return "#ffff00";
    if ( d >= 0.1 && d < 0.2 ) return "#fcd37f";
    if ( d >= 0.05 && d < 0.1 ) return "#ffaa00";
    if ( d >= 0.02 && d < 0.05 ) return "#e60000";
    if ( d >= 0.0 && d < 0.2 ) return "#730000";

    return "#fff";
  }

  onMapReady(map: L.Map) {
    map.on('baselayerchange', (eventLayer) => {
      const legend = new (L.Control.extend({
        options: { position: 'bottomright' }
      }));

      legend.onAdd = function (map) {
        const div = L.DomUtil.create('div', 'info legend');
        div.innerHTML +=
          '<div style="line-height: 18px; padding: 6px 8px;background: white; background: rgba(255,255,255,0.8); border-radius: 5px;">' +
            '<span><i style="background-color: #ffff00;width: 18px; height: 18px;display:inline-block;"></i> ungewöhnlich trocken (0,20 - 0,30)</span><br/>' +
            '<span><i style="background-color: #fcd37f;width: 18px; height: 18px;display:inline-block;"></i> moderate Dürre (0,10 - 0,20)</span><br/>' +
            '<span><i style="background-color: #ffaa00;width: 18px; height: 18px;display:inline-block;"></i> schwere Dürre (0,05 - 0,10)</span><br/>' +
            '<span><i style="background-color: #e60000;width: 18px; height: 18px;display:inline-block;"></i> extreme Dürre (0,02 - 0,05)</span><br/>' +
            '<span><i style="background-color: #730000;width: 18px; height: 18px;display:inline-block;"></i> außergewöhnliche Dürre (0,00 - 0,02)</span>' +
          '</div>';

        return div;
      };
      legend.addTo(map);
    });
  }
}
