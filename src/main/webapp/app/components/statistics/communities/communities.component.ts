import {Component, OnInit, ViewChild} from '@angular/core';
import {MASTR_COMMUNITIES_SUN} from "../../../config/data/mastr/mastr_communities_sun";
import {MASTR_COMMUNITIES_STORAGE} from "../../../config/data/mastr/mastr_communities_storage";
import {Table} from "primeng/table";

@Component({
  selector: 'jhi-communities',
  templateUrl: './communities.component.html',
  styleUrls: ['./communities.component.scss']
})
export class CommunitiesComponent implements OnInit {

  @ViewChild('sunTable') sunTable: Table | undefined;
  @ViewChild('storageTable') storageTable: Table | undefined;

  MASTR_COMMUNITIES_SUN_ALL = MASTR_COMMUNITIES_SUN;
  MASTR_COMMUNITIES_STORAGE_ALL = MASTR_COMMUNITIES_STORAGE;

  MASTR_COMMUNITIES_STORAGE: any[] = [];
  MASTR_COMMUNITIES_SUN: any[] = [];

  currentDate = new Date();
  communitySizes = [
    { minInhabitants : 0, maxInhabitants : 10000, label : "unter 10000 Einwohnende"},
    { minInhabitants : 10000, maxInhabitants : 100000, label : "zwischen 10.000 und 100.000 Einwohnende"},
    { minInhabitants : 100000, maxInhabitants : 1000000000, label : "über 100.000 Einwohnende"},
    { minInhabitants : 0, maxInhabitants : 1000000000, label : "Alle Größenklassen"}];
  selectedCommunitySizeStorage: any = this.communitySizes[3];
  selectedCommunitySizeForSun: any = this.communitySizes[3];
  sortedValues: any = {
    sunTable: {
      'inhabitants_2022_09_30' : [],
      'BruttoleistungTotal' : [],
      'BruttoleistungTotalPerInhabitants' : [],
      'BruttoleistungTotalBruttoleistungThisYearPerInhabitants' : [],  },
    storageTable: {
      'inhabitants_2022_09_30' : [],
      'BruttoleistungTotal' : [],
      'BruttoleistungTotalPerInhabitants' : [],
      'BruttoleistungTotalBruttoleistungThisYearPerInhabitants' : [],  }
  }

  constructor() { }

  getRank(table: Table, tableName: string, community: any): number {
    return table.sortField === 'name' ? null : (this.sortedValues[tableName][table.sortField]
      .findIndex((c : number) => c === community[table.sortField]) + 1)
  }

  ngOnInit(): void {

    MASTR_COMMUNITIES_STORAGE.communities.forEach(function(community : any){
      community.name = community.name.replace(", Stadt", "")
      community.BruttoleistungTotalPerInhabitants = community.BruttoleistungTotal / community.inhabitants_2022_09_30;
      community.BruttoleistungTotalBruttoleistungThisYearPerInhabitants = community.BruttoleistungThisYear / community.inhabitants_2022_09_30;
    })
    MASTR_COMMUNITIES_SUN.communities.forEach(function(community : any){
      community.name = community.name.replace(", Stadt", "")
      community.BruttoleistungTotalPerInhabitants = community.BruttoleistungTotal / community.inhabitants_2022_09_30;
      community.BruttoleistungTotalBruttoleistungThisYearPerInhabitants = community.BruttoleistungThisYear / community.inhabitants_2022_09_30;
    })

    for (const sortable of ['BruttoleistungTotal', 'BruttoleistungTotalPerInhabitants', 'BruttoleistungTotalBruttoleistungThisYearPerInhabitants', 'inhabitants_2022_09_30']) {
      for (const table of ['sunTable', 'storageTable']) {
        this.sortedValues[table][sortable] = [...new Set((table === 'sunTable' ? MASTR_COMMUNITIES_SUN : MASTR_COMMUNITIES_STORAGE).communities
          .map((community : any) => community[sortable]))]
          .sort((a, b) => b! - a!)
      }
    }

    this.filterPhotovoltaik();
    this.filterStorage();
  }

  filterPhotovoltaik() {
    this.MASTR_COMMUNITIES_SUN = this.MASTR_COMMUNITIES_SUN_ALL.communities.filter(community =>
      community.inhabitants_2022_09_30 >= this.selectedCommunitySizeForSun.minInhabitants &&
      community.inhabitants_2022_09_30 < this.selectedCommunitySizeForSun.maxInhabitants);
  }

  filterStorage() {
    this.MASTR_COMMUNITIES_STORAGE = this.MASTR_COMMUNITIES_STORAGE_ALL.communities.filter(community =>
      community.inhabitants_2022_09_30 >= this.selectedCommunitySizeStorage.minInhabitants &&
      community.inhabitants_2022_09_30 < this.selectedCommunitySizeStorage.maxInhabitants);
  }

  applyFilterGlobal(table: Table, $event: Event, stringVal: string) {
    table!.filterGlobal(($event.target as HTMLInputElement).value, stringVal);
  }
}
