import {faHome} from "@fortawesome/free-solid-svg-icons";

export const navbarData = [
  {
    routeLink: 'home',
    icon: faHome,
    label: 'Home'
  },
  {
    routeLink: 'erneuerbare-energien',
    icon: 'solar-panel',
    label: 'Erneuerbare'
  },
  {
    routeLink: 'treibhausgase',
    icon: 'fire',
    label: 'Treibhausgase'
  },
  {
    routeLink: 'klima',
    icon: 'temperature-arrow-up',
    label: 'Klima'
  },
  {
    routeLink: 'batteriespeicher',
    icon: 'battery-three-quarters',
    label: 'Batteriespeicher'
  },
  {
    routeLink: 'e-mobilität',
    icon: 'car',
    label: 'E-Mobilität'
  },
  {
    routeLink: 'wasserhaushalt',
    icon: 'water',
    label: 'Wasserhaushalt'
  },
  {
    routeLink: 'waldzustand',
    icon: 'tree',
    label: 'Waldzustand'
  },
  {
    routeLink: 'kommunen',
    icon: 'tree-city',
    label: 'Kommunen'
  },
  // {
  //   routeLink: 'einstellungen',
  //   icon: faGear,
  //   label: 'Einstellungen'
  // }
]
