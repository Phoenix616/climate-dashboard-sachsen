import { NgModule } from '@angular/core';

import { SharedLibsModule } from './shared-libs.module';
import { FindLanguageFromKeyPipe } from './language/find-language-from-key.pipe';
import { TranslateDirective } from './language/translate.directive';
import { AlertComponent } from './alert/alert.component';
import { AlertErrorComponent } from './alert/alert-error.component';
import { HasAnyAuthorityDirective } from './auth/has-any-authority.directive';
import { DurationPipe } from './date/duration.pipe';
import { FormatMediumDatetimePipe } from './date/format-medium-datetime.pipe';
import { FormatMediumDatePipe } from './date/format-medium-date.pipe';
import { SortByDirective } from './sort/sort-by.directive';
import { SortDirective } from './sort/sort.directive';
import { ItemCountComponent } from './pagination/item-count.component';
import { FilterComponent } from './filter/filter.component';
import { CardComponent } from '../components/shared/card/card.component';
import { RenewablesComponent } from "../components/statistics/renewables/renewables.component";
import { ClimateComponent } from "../components/statistics/climate/climate.component";
import { GreenhousegasComponent } from "../components/statistics/greenhousegas/greenhousegas.component";
import { StorageComponent } from "../components/statistics/storage/storage.component";
import { SaxonyMapComponent } from '../components/shared/map/saxony-map/saxony-map.component';
import { ChartCardComponent } from "../components/shared/chart-card/chart-card.component";
import { EmobilityComponent } from '../components/statistics/emobility/emobility.component';
import { OrderByPipe } from "./sort/order-by.pipe";
import { WaterComponent } from "../components/statistics/water/water.component";
import { ForestConditionComponent } from "../components/statistics/forest-condition/forest-condition.component";
import { CommunitiesComponent } from '../components/statistics/communities/communities.component';

@NgModule({
  imports: [SharedLibsModule],
  declarations: [
    FindLanguageFromKeyPipe,
    TranslateDirective,
    AlertComponent,
    AlertErrorComponent,
    HasAnyAuthorityDirective,
    DurationPipe,
    FormatMediumDatetimePipe,
    FormatMediumDatePipe,
    OrderByPipe,
    SortByDirective,
    SortDirective,
    ItemCountComponent,
    FilterComponent,
    CardComponent,
    RenewablesComponent,
    GreenhousegasComponent,
    ClimateComponent,
    StorageComponent,
    ChartCardComponent,
    SaxonyMapComponent,
    EmobilityComponent,
    WaterComponent,
    ForestConditionComponent,
    CommunitiesComponent
  ],
  exports: [
    SharedLibsModule,
    FindLanguageFromKeyPipe,
    TranslateDirective,
    AlertComponent,
    AlertErrorComponent,
    HasAnyAuthorityDirective,
    DurationPipe,
    FormatMediumDatetimePipe,
    FormatMediumDatePipe,
    OrderByPipe,
    SortByDirective,
    SortDirective,
    ItemCountComponent,
    FilterComponent,
    CardComponent,
    RenewablesComponent,
    GreenhousegasComponent,
    ClimateComponent,
    StorageComponent,
    ChartCardComponent,
    SaxonyMapComponent,
    EmobilityComponent,
    WaterComponent,
    ForestConditionComponent,
    CommunitiesComponent
  ],
})
export class SharedModule {}
