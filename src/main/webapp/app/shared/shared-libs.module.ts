import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { TranslateModule } from '@ngx-translate/core';
import { NgChartsModule } from 'ng2-charts';
import { NgSelectModule } from '@ng-select/ng-select';
import {TableModule} from 'primeng/table';
import { LeafletModule } from '@asymmetrik/ngx-leaflet';

@NgModule({
  exports: [FormsModule, CommonModule, NgbModule, InfiniteScrollModule,
    FontAwesomeModule, ReactiveFormsModule, TranslateModule, NgChartsModule,
    NgSelectModule, TableModule, LeafletModule],
})
export class SharedLibsModule {}
