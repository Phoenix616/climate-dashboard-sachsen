import { Injectable } from '@angular/core';
import { saxony_energy_data } from "../../config/data/saxony_energy_data";
import {GREEN_HOUSE_GAS_DATA, GREEN_HOUSE_GAS_IN_ATMOSPHERE} from "../../config/data/greenhousegas.constants";
import {STATISTICS} from "../../../app/config/data/mastr/mastr_statistics";
import {CHARGING_STATIONS_BY_TIME_UNIT} from "../../../app/config/data/bnetza/bnetza_charging_stations_by_time_unit";
import {RWTH_AACHEN_BATTERY_CAPACITY} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_capacity";
import {RWTH_AACHEN_BATTERY_CAPACITY_INCREASE} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_capacity_increase";
import {RWTH_AACHEN_BATTERY_POWER} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_power";
import {RWTH_AACHEN_BATTERY_POWER_INCREASE} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_power_increase";
import {RWTH_AACHEN_BATTERY_COUNT} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_count";
import {RWTH_AACHEN_BATTERY_TYPE} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_type";
import {RWTH_AACHEN_BATTERY_MEAN_CAPACITY} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_mean_capacity";
import {RWTH_AACHEN_BATTERY_CAPACITY_PLANNED} from "../../../app/config/data/rwth_aachen/rwth_aachen_battery_planned";
import {RENEWABLES_INCREASE_BY_TIME_UNIT} from "../../../app/config/data/mastr/mastr_renewables_by_time_unit";
import {MOBILITY_CHARTS_CAR_TYPE} from "../../../app/config/data/rwth_aachen/rwth_mobility_car_type";
import {DWD_TOTAL} from "../../config/data/dwd/dwd_total.constants";
import {CHARTJS_COLORS} from "../../config/colors.constants";
import {WALDZUSTANDSBERICHT_DATA} from "../../config/data/smekul/waldzustandsbericht.constants";

@Injectable({
  providedIn: 'root'
})
export class DataService {

  RWTH_AACHEN_BATTERY_POWER = RWTH_AACHEN_BATTERY_POWER;
  RWTH_AACHEN_BATTERY_CAPACITY = RWTH_AACHEN_BATTERY_CAPACITY;
  RWTH_AACHEN_BATTERY_CAPACITY_INCREASE = RWTH_AACHEN_BATTERY_CAPACITY_INCREASE;
  RWTH_AACHEN_BATTERY_POWER_INCREASE = RWTH_AACHEN_BATTERY_POWER_INCREASE;
  RWTH_AACHEN_BATTERY_COUNT = RWTH_AACHEN_BATTERY_COUNT;
  RWTH_AACHEN_BATTERY_MEAN_CAPACITY = RWTH_AACHEN_BATTERY_MEAN_CAPACITY;
  RWTH_AACHEN_BATTERY_CAPACITY_PLANNED = RWTH_AACHEN_BATTERY_CAPACITY_PLANNED;
  MOBILITY_CHARTS_CAR_TYPE = MOBILITY_CHARTS_CAR_TYPE;

  constructor() { }

  getLabels(physicalType: "work" | "power"){
    return saxony_energy_data[physicalType]['years'];
  }

  getData(physicalType: "work" | "power", eeType: "sun" | "wind" | "bio" | "water") {
    return saxony_energy_data[physicalType][eeType];
  }

  getLatestValue(physicalType: "work" | "power", eeType: "sun" | "wind" | "bio" | "water") {
    console.log(physicalType, eeType)
    return saxony_energy_data[physicalType][eeType][saxony_energy_data[physicalType][eeType].length - 1];
  }

  getLatestValueDiff(physicalType: "work" | "power", eeType: "sun" | "wind" | "bio" | "water") {
    const current = saxony_energy_data[physicalType][eeType][saxony_energy_data[physicalType][eeType].length - 1];
    const last = saxony_energy_data[physicalType][eeType][saxony_energy_data[physicalType][eeType].length - 2];
    return ((current / last * 100) - 100);
  }

  getMostRecentYear(physicalType: "work" | "power") {
    return saxony_energy_data[physicalType]['years'][saxony_energy_data[physicalType]['years'].length - 1];
  }

  getSumForYear(physicalType: "work" | "power", year: number) {
    const index = saxony_energy_data[physicalType]['years'].findIndex((element : number) => element === year);
    return saxony_energy_data[physicalType]['sun'][index] +
      saxony_energy_data[physicalType]['water'][index] +
      saxony_energy_data[physicalType]['bio'][index] +
      saxony_energy_data[physicalType]['wind'][index];
  }

  getStatistics() {
    return STATISTICS;
  }

  getLabels2(timeUnit: "m" | "y" | "w-mon") {
    return RENEWABLES_INCREASE_BY_TIME_UNIT["solare_strahlungsenergie_" + timeUnit].map((e : any) => e.TimeUnit);
  }

  getData2(timeUnit: "m" | "y" | "w-mon") {
    return RENEWABLES_INCREASE_BY_TIME_UNIT["solare_strahlungsenergie_" + timeUnit].map((e : any) => e.Bruttoleistung);
  }

  getBNetzALabels(timeUnit: "m" | "y" | "w-mon") {
    return CHARGING_STATIONS_BY_TIME_UNIT[timeUnit].map((e : any) => e.TimeUnit);
  }

  getBNetzAData(timeUnit: "m" | "y" | "w-mon", variableToDisplay: "TimeUnitChargingPoints" | "TimeUnitPowerRating" | "TotalCountChargingPoints" | "TotalChargingPower" ) {
    return CHARGING_STATIONS_BY_TIME_UNIT[timeUnit].map((e : any) => e[variableToDisplay]);
  }

  getGreenHouseGasData() {
    return GREEN_HOUSE_GAS_DATA;
  }

  getWaldzustandsBerichtData(treeType: "alleBaumarten") {
    return {
      source: WALDZUSTANDSBERICHT_DATA.source,
      labels: WALDZUSTANDSBERICHT_DATA.labels,
      datasets: WALDZUSTANDSBERICHT_DATA.datasets[treeType]
    }
  }

  getCo2InAtmosphereData() {
    return GREEN_HOUSE_GAS_IN_ATMOSPHERE;
  }

  getAverageTemperaturData() {
    return {
      labels: DWD_TOTAL.years,
      datasets: [
        {
          label: 'Jahresdurchschnittstemperatur',
          data: DWD_TOTAL.dwd_air_temperature_mean,
          backgroundColor: 'rgba(255, 99, 132, 1)',
          fill: false,
          borderColor: 'rgba(255, 99, 132, 1)'
        },{
          label: 'Durchschnittstemperatur (30 Jahre Mittel)',
            data: DWD_TOTAL.dwd_air_temperature_mean_rolling_30,
            backgroundColor: 'rgba(25, 99, 132, 1)',
            borderColor: 'rgba(25, 99, 132, 1)',
            fill: false
        }]
      };
  }

  getRWTHAachenMobilityData(rwthAachenDataKey: string): any {
    return {
      labels: (this as any)[rwthAachenDataKey].results.PHEV.frames[0].data.values[0],
      datasets: [
        {
          label: 'Batterieelektrisch',
          data: (this as any)[rwthAachenDataKey].results.BEV.frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[0]
        },
        {
          label: 'Plugin-Hybrid',
          data: (this as any)[rwthAachenDataKey].results.PHEV.frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[1]
        }
      ]
    };
  }

  getRWTHAachenData(rwthAachenDataKey: string): any {
    return {
      labels: (this as any)[rwthAachenDataKey].results.Gewerbespeicher.frames[0].data.values[0],
      datasets: [
        {
          label: 'Großspeicher',
          data: (this as any)[rwthAachenDataKey].results['Großspeicher'].frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[0]
        },
        {
          label: 'Gewerbespeicher',
          data: (this as any)[rwthAachenDataKey].results.Gewerbespeicher.frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[1]
        },
        {
          label: 'Heimspeicher',
          data: (this as any)[rwthAachenDataKey].results.Heimspeicher.frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[2]
        }
      ]
    };
  }

  getRWTHAachenBatteryCapacityPercentageData(): any {
    return {
      labels: RWTH_AACHEN_BATTERY_TYPE.results['Lithium-Ionen'].frames[0].data.values[0],
      datasets: [
        {
          label: 'Lithium-Ionen',
          data: RWTH_AACHEN_BATTERY_TYPE.results['Lithium-Ionen'].frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[0]
        },
        {
          label: 'Blei-Säure',
          data: RWTH_AACHEN_BATTERY_TYPE.results['Blei-Säure'].frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[1]
        },
        {
          label: 'Redox-Flow',
          data: RWTH_AACHEN_BATTERY_TYPE.results['Redox-Flow'].frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[2]
        },
        {
          label: 'Hochtemperatur',
          data: RWTH_AACHEN_BATTERY_TYPE.results['Hochtemperatur'].frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[3]
        },
        {
          label: 'NiCd/NiMH',
          data: RWTH_AACHEN_BATTERY_TYPE.results['NiCd/NiMH'].frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[4]
        },
        {
          label: 'Sonstige',
          data: RWTH_AACHEN_BATTERY_TYPE.results['Sonstige'].frames[0].data.values[1],
          fill: true,
          backgroundColor: CHARTJS_COLORS[5]
        },
      ]
    };
  }

  getRWTHAachenMeanBatteryPowerAndCapacityData() {
    return {
      labels: RWTH_AACHEN_BATTERY_MEAN_CAPACITY.results['Speicherkapazität'].frames[0].data.values[0],
      datasets: [
        {
          label: 'Speicherleistung',
          data: RWTH_AACHEN_BATTERY_MEAN_CAPACITY.results['Speicherkapazität'].frames[0].data.values[1],
        },
        {
          label: 'Speicherkapazität',
          data: RWTH_AACHEN_BATTERY_MEAN_CAPACITY.results['Speicherkapazität'].frames[0].data.values[2],
        }
      ]
    }
  }

  getRWTHAachenPlannedCapacityData() {
    return {
      labels: ["Heimspeicher", "Gewerbespeicher", "Großspeicher"],
      datasets: [
        { data: [ RWTH_AACHEN_BATTERY_CAPACITY_PLANNED.results.Heimspeicher.frames[0].data.values[1][0],
            RWTH_AACHEN_BATTERY_CAPACITY_PLANNED.results.Gewerbespeicher.frames[0].data.values[1][0],
            RWTH_AACHEN_BATTERY_CAPACITY_PLANNED.results['Großspeicher'].frames[0].data.values[1][0]] },
      ]
    }
  }
}
