/**
 * View Models used by Spring MVC REST controllers.
 */
package eu.danielgerber.web.rest.vm;
