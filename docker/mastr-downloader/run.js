import got from 'got';
import { Parser } from 'json2csv';
import * as fs from 'fs';

function fixData() {
  return (item) => ({
    ...item,
    SachsenId: item.Gemeindeschluessel.substring(0, 2),
    LandkreisId: item.Gemeindeschluessel.substring(2, 5),
    GemeindeId: item.Gemeindeschluessel.substring(5),
    InbetriebnahmeDatum: item.InbetriebnahmeDatum ?
      new Date(parseInt(item.InbetriebnahmeDatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : item.InbetriebnahmeDatum,
    EinheitMeldeDatum: item.EinheitMeldeDatum ?
      new Date(parseInt(item.EinheitMeldeDatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    DatumLetzteAktualisierung: item.DatumLetzteAktualisierung ?
      new Date(parseInt(item.DatumLetzteAktualisierung.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    EinheitRegistrierungsdatum: item.EinheitRegistrierungsdatum ?
      new Date(parseInt(item.EinheitRegistrierungsdatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    EndgueltigeStilllegungDatum: item.EndgueltigeStilllegungDatum ?
      new Date(parseInt(item.EndgueltigeStilllegungDatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    GeplantesInbetriebsnahmeDatum: item.GeplantesInbetriebsnahmeDatum ?
      new Date(parseInt(item.GeplantesInbetriebsnahmeDatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    EegInbetriebnahmeDatum: item.EegInbetriebnahmeDatum ?
      new Date(parseInt(item.EegInbetriebnahmeDatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    EegAnlageRegistrierungsdatum: item.EegAnlageRegistrierungsdatum ?
      new Date(parseInt(item.EegAnlageRegistrierungsdatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    GenehmigungDatum: item.GenehmigungDatum ?
      new Date(parseInt(item.GenehmigungDatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    GenehmigungRegistrierungsdatum: item.GenehmigungRegistrierungsdatum ?
      new Date(parseInt(item.GenehmigungRegistrierungsdatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    KwkAnlageInbetriebnahmedatum: item.KwkAnlageInbetriebnahmedatum ?
      new Date(parseInt(item.KwkAnlageInbetriebnahmedatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
    KwkAnlageRegistrierungsdatum: item.KwkAnlageRegistrierungsdatum ?
      new Date(parseInt(item.KwkAnlageRegistrierungsdatum.replace("/Date(", "").replace(")/", "")))
        .toISOString().slice(0, 10) : null,
  });
}

(async () => {
  try {
    const elements = [];
    let PAGE = 1;
    let PAGESIZE = 5000;

    while (true) {

      let url = `https://www.marktstammdatenregister.de/MaStR/Einheit/EinheitJson/GetErweiterteOeffentlicheEinheitStromerzeugung?page=${PAGE}&pageSize=${PAGESIZE}&group=&filter=Bundesland~eq~%271413%27`;

      console.log(elements.length, PAGE, url);
      const response = await got(url, { retry: { limit: 3, methods: ["GET"] }}).json();
      elements.push(...response.Data);
      PAGE++;

      if ( response.Data.length < PAGESIZE  ) {
        break;
      }
    }

    const csv = new Parser({ transforms: [fixData()] }).parse(elements);
    fs.writeFile('/app/data/mastr/mastr_sachsen_original.csv', csv, function (err,data) {
      if (err) { return console.log(err); }
    });

  } catch (error) {
    console.log(error);
  }
})();
