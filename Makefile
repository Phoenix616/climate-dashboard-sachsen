#############################
## Front Matter            ##
#############################

.PHONY: help

.DEFAULT_GOAL := help

#############################
## Targets                 ##
#############################

## build all docker images for this project
build: build-mastr-downloader build-build-container-image build-python

## build python image
build-python:
	docker build docker/python -f docker/python/Dockerfile -t klimadashboard/python-processor

## build the mastr download docker image only
build-mastr-downloader:
	docker build docker/mastr-downloader -f docker/mastr-downloader/Dockerfile -t klimadashboard/mastr-downloader

## builds the build container to build angular and java app
build-build-container-image:
	docker build -t klimadashboard/build-container-image -f docker/jhipster/Dockerfile .

## build nginx container with files to serve
build-nginx:
	docker build -t klimadashboard/dashboard-container -f docker/nginx/Dockerfile .

## build production version of frontend in ./target/classes/static
build-frontend-dev:
	docker run --rm -t -v $(shell pwd):/app build-container-image ./npmw run webapp:build:dev

## remove build files and data for clean build
clean:
	rm -rf target/classes
	rm -rf data/bnetza/* data/dwd/* data/mastr/* data/rwth_aachen/* data/ufz/*
	rm -rf src/main/webapp/app/config/data/bnetza/*
	rm -rf src/main/webapp/app/config/data/dwd/*
	rm -rf src/main/webapp/app/config/data/mastr/*
	rm -rf src/main/webapp/app/config/data/rwth_aachen/*
	rm -rf src/main/webapp/app/config/data/ufz/*

## Start the frontend for development
dev-frontend:
	./npmw start

## serve the build locally with nginx on 8007
build-and-serve-nginx:
	docker rm -f dashboard-container
	docker build -t dashboard-image -f docker/nginx/Dockerfile .
	docker run -it --rm --name dashboard-container -p 8007:80 dashboard-image

## build prod and run as nginx image
build-and-serve-nginx-prod:
	sudo -u $(SUDO_USER) ./npmw run build
	docker rm -f dashboard-container
	docker build -t dashboard-image -f docker/nginx/Dockerfile .
	docker run -d --restart=always --name dashboard-container -p 8007:80 dashboard-image

## download data, process data, build app, start container
build-download-data-dev: build download data build-frontend-dev build-nginx

## load all data from remote sources and process locally
download-and-data: download data

## process all data
data: data-dwd data-mastr data-bnetza data-rwth-aachen data-ufz

## process the already downloaded dwd data
data-dwd:
	rm -rf src/main/webapp/app/config/data/dwd/*
	docker run --rm -t -v $(shell pwd):/app klimadashboard/python-processor python /app/scripts/dwd/dwd-processor.py

## process the already downloaded marktstammdatenregister data
data-mastr:
	rm -rf src/main/webapp/app/config/data/mastr/*
	docker run --rm -t -v $(shell pwd):/app klimadashboard/python-processor python /app/scripts/mastr/mastr-processor.py

# FIXME make target "data-bnetza" independant from target "data-mastr"
## process the already downloaded BNetzA data
data-bnetza:
	rm -rf src/main/webapp/app/config/data/bnetza/*
	docker run --rm -t -v $(shell pwd):/app klimadashboard/python-processor python /app/scripts/bnetza/bnetza-processor.py

## prepare the rwth aachen data (battery storage and e-mobility)
data-rwth-aachen:
	rm -rf src/main/webapp/app/config/data/rwth_aachen/*
	docker run --rm -t -v $(shell pwd)/scripts/:/app/scripts -w=/app/scripts/rwth_aachen -v $(shell pwd)/src/main/webapp/app/config/data/:/app/data/ klimadashboard/mastr-downloader node run-battery.js
	docker run --rm -t -v $(shell pwd)/scripts/:/app/scripts -w=/app/scripts/rwth_aachen -v $(shell pwd)/src/main/webapp/app/config/data/:/app/data/ klimadashboard/mastr-downloader node run-mobility.js

## process the already downloaded ufz data (duerremonitor)
data-ufz:
	rm -rf src/main/webapp/app/config/data/ufz/*
	docker run --rm -t -v $(shell pwd):/app klimadashboard/python-processor python /app/scripts/ufz/ufz-processor.py

## download all data
download: download-mastr download-dwd download-bnetza download-ufz

## download the dwd data
download-dwd:
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/frost_days/regional_averages_tnas_year.txt -O /wget/dwd_frost_days.csv
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/air_temperature_mean/regional_averages_tm_year.txt -O /wget/dwd_air_temperature_mean.csv
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/hot_days/regional_averages_txbs_year.txt -O /wget/dwd_hot_days.csv
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/ice_days/regional_averages_txcs_year.txt -O /wget/dwd_ice_days.csv
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/precipGE10mm_days/regional_averages_rrsfs_year.txt -O /wget/dwd_precipGE10mm_days.csv
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/precipGE20mm_days/regional_averages_rrsgs_year.txt -O /wget/dwd_precipGE20mm_days.csv
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/precipitation/regional_averages_rr_year.txt -O /wget/dwd_precipitation.csv
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/summer_days/regional_averages_txas_year.txt -O /wget/dwd_summer_days.csv
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/sunshine_duration/regional_averages_sd_year.txt -O /wget/dwd_sunshine_duration.csv
	docker run --rm -t -v $(shell pwd)/data/dwd/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://opendata.dwd.de/climate_environment/CDC/regional_averages_DE/annual/tropical_nights_tminGE20/regional_averages_tnes_year.txt -O /wget/dwd_tropical_nights_tminGE20.csv

## download the marktstammdatenregister data
download-mastr:
	docker run --rm -t -v $(shell pwd)/docker:/app/docker -v $(shell pwd)/data/mastr:/app/data/mastr/ klimadashboard/mastr-downloader node /app/docker/mastr-downloader/run.js

## download the bundesnetzagentur data (e-ladesaeulen)
download-bnetza:
	docker run --rm -t -v $(shell pwd)/data/bnetza/:/wget/ cirrusci/wget:latest wget --no-check-certificate\
		https://www.bundesnetzagentur.de/SharedDocs/Downloads/DE/Sachgebiete/Energie/Unternehmen_Institutionen/E_Mobilitaet/Ladesaeulenregister.xlsx?__blob=publicationFile -O /wget/bnetza-ladesäulen.xlsx

## download the ufz data (duerremonitor)
download-ufz:
	docker run --rm -t -v $(shell pwd)/data/ufz/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://files.ufz.de/~drought/SM_L02_daily_n14.nc -O /wget/smi_oberboden_daily_14d.nc
	docker run --rm -t -v $(shell pwd)/data/ufz/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://files.ufz.de/~drought/SM_Lall_daily_n14.nc -O /wget/smi_gesamtboden_daily_14d.nc
	docker run --rm -t -v $(shell pwd)/data/ufz/:/wget/ cirrusci/wget:latest wget --no-check-certificate https://files.ufz.de/~drought/nFK_0_25_daily_n14.nc -O /wget/pflanzenverfuegbares_wasser_daily_14d.nc

## Run unit and e2e tests (needs running application)
test-all: test-unit test-e2e

## Run backend and frontend unit tests
test-unit: clean
	./npmw test

## Run end-to-end tests (needs running application)
test-e2e:
	./npmw run e2e

#############################
## Help Target             ##
#############################

## Show this help
help:
	@printf "Available targets:\n\n"
	@awk '/^[a-zA-Z\-\_0-9]+:/ { \
		helpMessage = match(lastLine, /^## (.*)/); \
		if (helpMessage) { \
			helpCommand = substr($$1, 0, index($$1, ":")-1); \
			helpMessage = substr(lastLine, RSTART + 3, RLENGTH); \
			printf "  %-40s%s\n", helpCommand, helpMessage; \
		} \
	} \
	{ lastLine = $$0 }' $(MAKEFILE_LIST) | sort
